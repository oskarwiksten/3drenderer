#include "DObject.h"
#include "util.h"

void DSurfaceObject::getSurfaces(SurfaceCollection &dest) const {
	std::copy(surfaces.begin(), surfaces.end(), std::back_inserter(dest));
}

void DSurfaceObject::print(std::ostream &os, unsigned int indent) const {
	for(SurfaceCollection::const_iterator i = surfaces.begin(); i != surfaces.end(); ++i) {
		i->print(os, indent);
	}
}

void DSurfaceObject::scale(float f) {
	for(SurfaceCollection::iterator i = surfaces.begin(); i != surfaces.end(); ++i) {
		i->scale(f);
	}
}

void DSurfaceObject::addSurfaceRecursive(const Vertex &v0, const Vertex &v1, const Vertex &v2, unsigned int recursions, const Material &material, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2) {
	//if (recursions == 0) {
		Surface s(this, Surface::VertexSet(v0, v1, v2), Surface::NormalSet(normal_v0, normal_v1, normal_v2), material);
		//std::cout << "Adding surface " << s.vertices << " , normals=" << s.normals << " , getCenterUnitNormal()=" << s.getCenterUnitNormal() << std::endl;
		surfaces.push_back(s);
		return;
	//}
	/*
	 *   v2 +----------, v1
	 *      |    ..--''    
	 *      +--''
	 *     v0
	 */
	/*--recursions;
	const Vertex v0v1 = v1 - v0;
	const Vertex v1v2 = v2 - v1;
	const Vertex v2v0 = v0 - v2;	
	float len_v0v1 = v0v1.lengthSquared();
	float len_v1v2 = v1v2.lengthSquared();
	float len_v2v0 = v2v0.lengthSquared();
	if (len_v0v1 > len_v1v2 && len_v0v1 > len_v2v0) {
		const Vertex m = v0 + (v0v1/2);
		addSurfaceRecursive(v0, m, v2, recursions, normal_v0, normal_v1, normal_v2);
		addSurfaceRecursive(v2, m, v1, recursions, normal_v0, normal_v1, normal_v2);
	} else if (len_v1v2 > len_v0v1 && len_v1v2 > len_v2v0) {
		const Vertex m = v1 + (v1v2/2);
		addSurfaceRecursive(v1, m, v0, recursions, normal_v0, normal_v1, normal_v2);
		addSurfaceRecursive(v0, m, v2, recursions, normal_v0, normal_v1, normal_v2);
	} else {
		const Vertex m = v2 + (v2v0/2);
		addSurfaceRecursive(v2, m, v1, recursions, normal_v0, normal_v1, normal_v2);
		addSurfaceRecursive(v1, m, v0, recursions, normal_v0, normal_v1, normal_v2);
	}*/	
}

unsigned int DSurfaceObject::getNumberOfVertices() const {
	return surfaces.size() * 3;
}

void DSurfaceObject::setColor(const PixelColor &c) { 
	for(SurfaceCollection::iterator i = surfaces.begin(); i != surfaces.end(); ++i) {
		i->material.baseColor = c;
	}
}

void DSurfaceObject::addSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Material &material) {
	const NormalVector normal = createUnitNormal(DSurface::VertexSet(v0, v1, v2));
	addSurface(v0, v1, v2, material, normal, normal, normal);
}

void DSurfaceObject::addSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Material &material, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2) {
	addSurfaceRecursive(v0, v1, v2, 0, material, normal_v0, normal_v1, normal_v2);
}

void DSurfaceObject::addSquareSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Vertex &v3, const PixelColor &baseColor, const Texture *texture, const Texture *bumpmapTexture) {
	const NormalVector normal = createUnitNormal(DSurface::VertexSet(v0, v1, v2));
	addSquareSurface(v0, v1, v2, v3, normal, normal, normal, normal, baseColor, texture, bumpmapTexture);
}

void DSurfaceObject::addSquareSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Vertex &v3, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2, const NormalVector &normal_v3, const PixelColor &baseColor, const Texture *texture, const Texture *bumpmapTexture) {
	if (texture != NULL && bumpmapTexture != NULL) {
		addSquareSurface(
			v0, v1, v2, v3
			, normal_v0, normal_v1, normal_v2, normal_v3
			, baseColor
			, texture
			, texture->topLeft(), texture->topRight(), texture->bottomRight(), texture->bottomLeft()
			, bumpmapTexture
			, bumpmapTexture->topLeft(), bumpmapTexture->topRight(), bumpmapTexture->bottomRight(), bumpmapTexture->bottomLeft()
		);
	} else {
		addSurface(v0, v1, v2, Material(baseColor), normal_v0, normal_v1, normal_v2);
		addSurface(v2, v3, v0, Material(baseColor), normal_v2, normal_v3, normal_v0);
	}
}

void DSurfaceObject::addSquareSurface(
		const Vertex &v0, const Vertex &v1, const Vertex &v2, const Vertex &v3
		, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2, const NormalVector &normal_v3
		, const PixelColor &baseColor
		, const Texture *texture
		, const Texture::TexelPosition &tp0, const Texture::TexelPosition &tp1, const Texture::TexelPosition &tp2, const Texture::TexelPosition &tp3
		, const Texture *bumpmapTexture
		, const Texture::TexelPosition &btp0, const Texture::TexelPosition &btp1, const Texture::TexelPosition &btp2, const Texture::TexelPosition &btp3
		) {
	Material m0(baseColor, texture, Material::TexturePositionSet(tp0, tp1, tp2), bumpmapTexture, Material::TexturePositionSet(btp0, btp1, btp2));
	addSurface(v0, v1, v2, m0, normal_v0, normal_v1, normal_v2);
	Material m1(baseColor, texture, Material::TexturePositionSet(tp2, tp3, tp0), bumpmapTexture, Material::TexturePositionSet(btp2, btp3, btp0));
	addSurface(v2, v3, v0, m1, normal_v2, normal_v3, normal_v0);
}

