#ifndef __DObject_h__
#define __DObject_h__

#include <SDL/SDL.h>
#include <vector>
#include <map>
#include <string>

#include "Material.h"
#include "MDVector.h"
#include "Rotation.h"
#include "DSurface.h"

class DObjectCollection;

class DObject {
public:
	typedef DSurface Surface;
	typedef Surface::Vertex Vertex;
	typedef Surface::NormalVector NormalVector;
	typedef Displacement::Rotation Rotation;
	typedef Displacement::Direction Direction;
	typedef std::vector<Surface> SurfaceCollection;
	typedef DSurface::RenderMode::Mode RenderMode;

	DObjectCollection *container;

	DObject() : container(NULL) {}
	virtual ~DObject() {}
	virtual void getSurfaces(SurfaceCollection &dest) const = 0;
	virtual unsigned int getNumberOfVertices() const { return 0; }
	virtual void setColor(const PixelColor &c) = 0;
	virtual void setRenderMode(const RenderMode &m) = 0;
	virtual void scale(float f) = 0;
};

class DSurfaceObject : public DObject {
private:
	//TODO: the object should contain a set of all vertexes, so that rotations can be done on the 
	//       set instead of on multiple copies of each vertex.
	SurfaceCollection surfaces;
	void addSurfaceRecursive(const Vertex &v0, const Vertex &v1, const Vertex &v2, unsigned int recursions, const Material &material, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2);

	RenderMode renderMode;
	
public:
	DSurfaceObject(const RenderMode renderMode_ = DSurface::RenderMode::SURFACE) 
		: renderMode(renderMode_) {
	}
	virtual ~DSurfaceObject() {}
	virtual void scale(float f);
	
	const SurfaceCollection &getSurfaces() const { return surfaces; }
	virtual void getSurfaces(SurfaceCollection &dest) const;
	void addSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Material &material);
	void addSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Material &material, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2);
	void addSquareSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Vertex &v3, const PixelColor &baseColor, const Texture *texture = NULL, const Texture *bumpmapTexture = NULL);
	void addSquareSurface(const Vertex &v0, const Vertex &v1, const Vertex &v2, const Vertex &v3, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2, const NormalVector &normal_v3, const PixelColor &baseColor, const Texture *texture = NULL, const Texture *bumpmapTexture = NULL);
	void addSquareSurface(
		  const Vertex &v0, const Vertex &v1, const Vertex &v2, const Vertex &v3
		, const NormalVector &normal_v0, const NormalVector &normal_v1, const NormalVector &normal_v2, const NormalVector &normal_v3
		, const PixelColor &baseColor
		, const Texture *texture
		, const Texture::TexelPosition &tp0, const Texture::TexelPosition &tp1, const Texture::TexelPosition &tp2, const Texture::TexelPosition &tp3
		, const Texture *bumpmapTexture
		, const Texture::TexelPosition &btp0, const Texture::TexelPosition &btp1, const Texture::TexelPosition &btp2, const Texture::TexelPosition &btp3
	);
	virtual unsigned int getNumberOfVertices() const;

	//PixelColor getColor() const { return objectColor; }
	RenderMode getRenderMode() const { return renderMode; }
	virtual void setColor(const PixelColor &c);
	virtual void setRenderMode(const RenderMode &m) { renderMode = m; }

	void print(std::ostream &os, unsigned int indent = 0) const;
};

#endif
