#include "DObjectCollection.h"
#include "util.h"

void DObjectCollection::getSurfaces(SurfaceCollection &dest) const {
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		SurfaceCollection::size_type oldsize = dest.size();
		//SurfaceCollection::iterator last = dest.end();
		//--last;
		(*i)->object->getSurfaces(dest);
		for (SurfaceCollection::size_type j = oldsize; j < dest.size(); ++j) {
			dest[j].apply((*i)->displacement);
		}
		/*for (++last; last != dest.end(); ++last) {
			last->apply((*i)->displacement);
		}*/
	}
}

DObjectCollection::~DObjectCollection() {
	for (ObjectList::iterator i = objects.begin(); i != objects.end(); ++i) {
		DisplacedObject* o = *i;
		delete o->object;
		delete o;
	}
}

DObjectCollection::DisplacedObject* DObjectCollection::add(DObject *object, const Displacement &displacement, const std::string &name) {
	DisplacedObject *o = new DisplacedObject();
	o->object = object;
	o->displacement = displacement;
	object->container = this;
	
	objects.push_back(o);
	if (!name.empty()) {
		namedObjects.insert(std::make_pair(name, o));
	}
	
	return o;
}

void DObjectCollection::print(std::ostream &os, unsigned int indent) const {
	ind(os, indent);
	os << "DObjectCollection: &DObject = " << this << std::endl;
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		DisplacedObject* o = *i;
		ind(os, indent);
		os << " &DisplacedObject = " << o << "   &DObject = " << o->object << std::endl;
	}
	ind(os, indent);
	os << "NamedObjectMap" << std::endl;
	for (NamedObjectMap::const_iterator j = namedObjects.begin(); j != namedObjects.end(); ++j) {
		DisplacedObject* o = j->second;
		ind(os, indent);
		os << " &DisplacedObject = " << o << "   &DObject = " << o->object << "   (" << j->first << ")" << std::endl;
		if (o != NULL && o->object != NULL) {
			ind(os, indent);
			os << " rotation=" << o->displacement.rotation << ", translation=" << o->displacement.translation << std::endl;
			const DSurfaceObject *surfaceObject = dynamic_cast<const DSurfaceObject*>(o->object);
			if (surfaceObject != NULL) {
				surfaceObject->print(os, indent + 2);
			}
			const DObjectCollection *collection = dynamic_cast<const DObjectCollection*>(o->object);
			if (collection != NULL) {
				collection->print(os, indent + 4);
			}
		}
	}
}
	
DObjectCollection::DisplacedObject* DObjectCollection::findObject(const std::string &name) {
	return const_cast<DisplacedObject*>(const_cast<const DObjectCollection*>(this)->findObject(name));
}

const DObjectCollection::DisplacedObject* DObjectCollection::findObject(const std::string &name) const {
	std::string::size_type p = name.find('/');
	if (p == std::string::npos) {
		NamedObjectMap::const_iterator i = namedObjects.find(name);
		if (i == namedObjects.end()) return NULL;
		return i->second;
	} else {
		NamedObjectMap::const_iterator i = namedObjects.find(name.substr(0, p));
		if (i == namedObjects.end()) return NULL;
		const DisplacedObject *o = i->second;
		++p;
		if (p >= name.length()) return o;
		
		const DObjectCollection *collection = dynamic_cast<const DObjectCollection*>(o->object);
		if (collection == NULL) return o;
		return collection->findObject(name.substr(p));
	}
	return NULL;
}

DObjectCollection::DisplacedObject* DObjectCollection::findObject(const DObject *object) {
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		DObjectCollection::DisplacedObject* o = *i;
		if (o->object == object) return o;
	}
	return NULL;
}


unsigned int DObjectCollection::getNumberOfVertices() const {
	unsigned int result = 0;
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		result += (*i)->object->getNumberOfVertices();
	}
	return result;
}

void DObjectCollection::setColor(const PixelColor &c) {
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		(*i)->object->setColor(c);
	}
}

void DObjectCollection::setRenderMode(const RenderMode &m) {
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		(*i)->object->setRenderMode(m);
	}
}

DObjectCollection::DisplacedObject *getDisplacedObject(const DObject *obj) {
	if (obj == NULL) return NULL;
	if (obj->container == NULL) return NULL;
	return obj->container->findObject(obj);
}

void DObjectCollection::scale(float f) {
	for (ObjectList::const_iterator i = objects.begin(); i != objects.end(); ++i) {
		(*i)->object->scale(f);
	}
}

