#ifndef __DObjectCollection_h__
#define __DObjectCollection_h__

#include <SDL/SDL.h>
#include <vector>
#include <map>
#include <string>

#include "Rotation.h"
#include "DObject.h"

class DObjectCollection : public DObject {
public:
	typedef struct {
		DObject *object;
		Displacement displacement;
	} DisplacedObject;
	typedef std::vector<DisplacedObject*> ObjectList;
	
private:
	typedef std::map<std::string, DisplacedObject*> NamedObjectMap;
	ObjectList objects;
	NamedObjectMap namedObjects;
	DObjectCollection() {}

public:
	static DObjectCollection *create() { return new DObjectCollection(); }
	virtual ~DObjectCollection();
	virtual void getSurfaces(SurfaceCollection &dest) const;
	const ObjectList& getObjects() const { return objects; }

	DisplacedObject* add(DObject *object) { return add(object, Displacement::zero, ""); }
	DisplacedObject* add(DObject *object, const Displacement &displacement) { return add(object, displacement, ""); }
	DisplacedObject* add(DObject *object, const std::string &name) { return add(object, Displacement::zero, name); }
	DisplacedObject* add(DObject *object, const Displacement &displacement, const std::string &name);
	
	const DisplacedObject* findObject(const std::string &name) const;
	DisplacedObject* findObject(const std::string &name);
	DisplacedObject* findObject(const DObject *object);
	
	virtual unsigned int getNumberOfVertices() const;
	virtual void setColor(const PixelColor &c);
	virtual void setRenderMode(const RenderMode &m);
	
	void print(std::ostream &os, unsigned int indent = 0) const;
	virtual void scale(float f);
};

DObjectCollection::DisplacedObject *getDisplacedObject(const DObject *obj);


#endif
