#include "DSolids.h"

DObject *DCube::create(Uint16 dimensions[3], const PixelColor &objectColor, const Texture *texture, const Texture *bumpmapTexture) {
	/*
	   v1--------v2                        A  dimensions[1]
	  / |       / |                        |
	v3--------v4  |                        |
	 |  |      |  |                        |
	 | v5------|-v6                        |
	 |/        |/                          |
	v7--------v8                           +------------> dimensions[0]
	                                      /
	                                     /
	                                    V  dimensions[2]
	*/
	
	DSurfaceObject *obj = new DSurfaceObject();
	
	Vertex v1(
			 -dimensions[0] / 2
			, dimensions[1] / 2
			,-dimensions[2] / 2
		);
	Vertex v2(v1);
	v2[0] += dimensions[0];
	Vertex v3(v1);
	v3[2] += dimensions[2];
	Vertex v4(v3);
	v4[0] += dimensions[0];
	
	Vertex v5(v1);
	v5[1] -= dimensions[1];
	Vertex v6(v2);
	v6[1] -= dimensions[1];
	Vertex v7(v3);
	v7[1] -= dimensions[1];
	Vertex v8(v4);
	v8[1] -= dimensions[1];
	
	obj->addSquareSurface(v1,v2,v4,v3, objectColor, texture, bumpmapTexture); // top
	obj->addSquareSurface(v1,v3,v7,v5, objectColor, texture, bumpmapTexture); // left
	obj->addSquareSurface(v2,v6,v8,v4, objectColor, texture, bumpmapTexture); // right
	obj->addSquareSurface(v4,v8,v7,v3, objectColor, texture, bumpmapTexture); // front
	obj->addSquareSurface(v6,v2,v1,v5, objectColor, texture, bumpmapTexture); // back
	obj->addSquareSurface(v6,v5,v7,v8, objectColor, texture, bumpmapTexture); // bottom
	return obj;
}

DObject *DCylinder::create(Uint16 length, Uint16 outerRadius, Uint16 innerRadius, Uint16 circularPoints, const PixelColor &objectColor) {
	DSurfaceObject *obj = new DSurfaceObject();
	
	const Uint16 x = length / 2;
	Vertex lastRightOuterPoint(x, outerRadius, 0);
	Vertex lastLeftOuterPoint(-x, outerRadius, 0);
	Vertex lastRightInnerPoint(x, innerRadius, 0);
	Vertex lastLeftInnerPoint(-x, innerRadius, 0);
	NormalVector lastOuterNormal(0, 10, 0);
	NormalVector lastInnerNormal(-lastInnerNormal);
	NormalVector rightSideNormal(10, 0, 0);
	NormalVector leftSideNormal(-rightSideNormal);
	
	// 1 2 3 4
	// 4 3 2 1
	// 3 2 1 4
	for (Uint16 i = 1; i <= circularPoints; ++i) {
		const float v = i * M_2PI / circularPoints;
		
		Vertex currentRightOuterPoint(x, outerRadius, 0);
		Vertex currentLeftOuterPoint(-x, outerRadius, 0);
		Vertex currentRightInnerPoint(x, innerRadius, 0);
		Vertex currentLeftInnerPoint(-x, innerRadius, 0);
		NormalVector outerNormal(0, 10, 0);
		NormalVector innerNormal(-outerNormal);
	
		Rotation rotation(v, 0, 0);
		Displacement displacement(Direction::zero, rotation);
		displacement.applyTo(currentRightOuterPoint);
		displacement.applyTo(currentLeftOuterPoint);
		displacement.applyTo(currentRightInnerPoint);
		displacement.applyTo(currentLeftInnerPoint);
		displacement.applyTo(outerNormal);
		displacement.applyTo(innerNormal);
		
		// Outer tube
		obj->addSquareSurface(currentRightOuterPoint, lastRightOuterPoint, lastLeftOuterPoint, currentLeftOuterPoint, outerNormal, lastOuterNormal, lastOuterNormal, outerNormal, objectColor);
		// Inner tube
		obj->addSquareSurface(currentLeftInnerPoint, lastLeftInnerPoint, lastRightInnerPoint, currentRightInnerPoint, innerNormal, lastInnerNormal, lastInnerNormal, innerNormal, objectColor);
		// Right end side
		obj->addSquareSurface(currentRightInnerPoint, lastRightInnerPoint, lastRightOuterPoint, currentRightOuterPoint, rightSideNormal, rightSideNormal, rightSideNormal, rightSideNormal, objectColor);
		// Left end side
		obj->addSquareSurface(currentLeftOuterPoint, lastLeftOuterPoint, lastLeftInnerPoint, currentLeftInnerPoint, leftSideNormal, leftSideNormal, leftSideNormal, leftSideNormal, objectColor);
		
		lastLeftOuterPoint = currentLeftOuterPoint;
		lastRightOuterPoint = currentRightOuterPoint;
		lastLeftInnerPoint = currentLeftInnerPoint;
		lastRightInnerPoint = currentRightInnerPoint;
		lastOuterNormal = outerNormal;
		lastInnerNormal = innerNormal;
	}
	return obj;
}

template<class V>
void rotate(V &v, const Uint8 hstep, const Uint8 vstep, const Uint8 hlevels, const Uint8 vlevels) {
	Displacement verticalrotation(Displacement::Direction::zero, Displacement::Rotation(-M_PI * vstep / vlevels, 0, 0));
	verticalrotation.applyTo(v);
	
	Displacement horizontalrotation(Displacement::Direction::zero, Displacement::Rotation(0, -2 * M_PI * hstep / hlevels, 0));
	horizontalrotation.applyTo(v);
}

DObject *DSphere::create(Uint16 radius, Uint8 hlevels, Uint8 vlevels, const PixelColor &objectColor) {
	/*
	
		     .
		    / \
		   +---+
		  /     \
	     +-------+
	*/
	
	DSurfaceObject *obj = new DSurfaceObject();
	const Uint8 rotationalFaces = hlevels;
	const Uint8 verticalFaces = vlevels;
	assert(hlevels >= 4);
	assert(vlevels >= 2);
	
	const Vertex top(0, radius, 0);
	const Vertex bottom(0, -radius, 0);
	const NormalVector topNormal(0, 1, 0);
	const NormalVector bottomNormal(0, -1, 0);
	const Material material(objectColor);
	
	// Calculate initial top cone (level) vertices
	Vertex lastLevelVertices[rotationalFaces];
	Vertex levelVertices[rotationalFaces];
	NormalVector lastLevelNormals[rotationalFaces];
	NormalVector levelNormals[rotationalFaces];
	for (Uint8 h = 0; h < rotationalFaces; ++h) {
		levelVertices[h] = top;
		rotate(levelVertices[h], h, 1, hlevels, vlevels);
		
		levelNormals[h] = topNormal;
		rotate(levelNormals[h], h, 1, hlevels, vlevels);
	}
	
	// Add top cone
	obj->addSurface(top, levelVertices[0], levelVertices[rotationalFaces-1], material, topNormal, levelNormals[0], levelNormals[rotationalFaces-1]);
	for (Uint8 h = 1; h < rotationalFaces; ++h) {
		obj->addSurface(top, levelVertices[h], levelVertices[h-1], material, topNormal, levelNormals[h], levelNormals[h-1]);
	}
	
	// Add vertical levels
	for(Uint8 v = 1; v < verticalFaces-1; ++v) {
		for (Uint8 h = 0; h < rotationalFaces; ++h) {
			lastLevelVertices[h] = levelVertices[h];
			levelVertices[h] = top;
			rotate(levelVertices[h], h, v+1, hlevels, vlevels);
			
			lastLevelNormals[h] = levelNormals[h];
			levelNormals[h] = topNormal;
			rotate(levelNormals[h], h, v+1, hlevels, vlevels);
		}
		
		obj->addSquareSurface(lastLevelVertices[rotationalFaces-1], lastLevelVertices[0], levelVertices[0], levelVertices[rotationalFaces-1], lastLevelNormals[rotationalFaces-1], lastLevelNormals[0], levelNormals[0], levelNormals[rotationalFaces-1], objectColor);
		for (Uint8 h = 1; h < rotationalFaces; ++h) {
			obj->addSquareSurface(lastLevelVertices[h-1], lastLevelVertices[h], levelVertices[h], levelVertices[h-1], lastLevelNormals[h-1], lastLevelNormals[h], levelNormals[h], levelNormals[h-1], objectColor);
		}
	}
	
	// Add bottom cone
	obj->addSurface(levelVertices[0], bottom, levelVertices[rotationalFaces-1], material, levelNormals[0], bottomNormal, levelNormals[rotationalFaces-1]);
	for (Uint8 h = 1; h < rotationalFaces; ++h) {
		obj->addSurface(levelVertices[h], bottom, levelVertices[h-1], material, levelNormals[h], bottomNormal, levelNormals[h-1]);
	}
	return obj;
}

DObject *DSpiral::create(Uint16 length, Uint16 radius, Uint16 width, float twists, Uint16 points, const PixelColor &objectColor) {
	DSurfaceObject *obj = new DSurfaceObject();
	ObjectCoordinate y = length/2;
	Vertex lastRightBack(radius, y, width/2);
	Vertex lastLeftBack(-radius, y, width/2);
	Vertex lastRightFront(radius, y, -width/2);
	Vertex lastLeftFront(-radius, y, -width/2);
	
	obj->addSquareSurface(lastRightBack, lastLeftBack, lastLeftFront, lastRightFront, objectColor);
	for (Uint16 i = 1; i <= points; ++i) {
		y = static_cast<ObjectCoordinate>(length/2 - static_cast<float>(i) * length / points);
		const float v = i * M_PI * twists / points;
		
		Vertex currentRightBack(radius, y, width/2);
		Vertex currentLeftBack(-radius, y, width/2);
		Vertex currentRightFront(radius, y, -width/2);
		Vertex currentLeftFront(-radius, y, -width/2);
		
		Rotation rotation(0, v, 0);
		Displacement displacement(Direction::zero, rotation);
		displacement.applyTo(currentRightBack);
		displacement.applyTo(currentLeftBack);
		displacement.applyTo(currentRightFront);
		displacement.applyTo(currentLeftFront);
		
		obj->addSquareSurface(lastRightFront, lastLeftFront, currentLeftFront, currentRightFront, objectColor); // front
		obj->addSquareSurface(currentRightBack, currentLeftBack, lastLeftBack, lastRightBack, objectColor); // back
		obj->addSquareSurface(currentRightFront, currentRightBack, lastRightBack, lastRightFront, objectColor); // right side
		obj->addSquareSurface(lastLeftFront, lastLeftBack, currentLeftBack, currentLeftFront, objectColor); // left side
		
		lastRightBack = currentRightBack;
		lastLeftBack = currentLeftBack;
		lastRightFront = currentRightFront;
		lastLeftFront = currentLeftFront;
	}
	obj->addSquareSurface(lastRightFront, lastLeftFront, lastLeftBack, lastRightBack, objectColor);
	
	return obj;
}

typedef float f3point[3];

/* 306 vertices */
f3point vertices[306]={{1.4 , 0.0 , 2.4}, {1.4 , -0.784 , 2.4},
{0.784 , -1.4 , 2.4}, {0.0 , -1.4 , 2.4}, {1.3375 , 0.0 , 2.53125},
{1.3375 , -0.749 , 2.53125}, {0.749 , -1.3375 , 2.53125}, {0.0 , -1.3375 , 2.53125}, 
{1.4375 , 0.0 , 2.53125}, {1.4375 , -0.805 , 2.53125}, {0.805 , -1.4375 , 2.53125},
{0.0 , -1.4375 , 2.53125}, {1.5 , 0.0 , 2.4}, {1.5 , -0.84 , 2.4},
{0.84 , -1.5 , 2.4}, {0.0 , -1.5 , 2.4}, {-0.784 , -1.4 , 2.4},
{-1.4 , -0.784 , 2.4}, {-1.4 , 0.0 , 2.4}, {-0.749 , -1.3375 , 2.53125},
{-1.3375 , -0.749 , 2.53125}, {-1.3375 , 0.0 , 2.53125}, {-0.805 , -1.4375 , 2.53125},
{-1.4375 , -0.805 , 2.53125}, {-1.4375 , 0.0 , 2.53125}, {-0.84 , -1.5 , 2.4},
{-1.5 , -0.84 , 2.4}, {-1.5 , 0.0 , 2.4}, {-1.4 , 0.784 , 2.4},
{-0.784 , 1.4 , 2.4}, {0.0 , 1.4 , 2.4}, {-1.3375 , 0.749 , 2.53125},
{-0.749 , 1.3375 , 2.53125}, {0.0 , 1.3375 , 2.53125}, {-1.4375 , 0.805 , 2.53125},
{-0.805 , 1.4375 , 2.53125}, {0.0 , 1.4375 , 2.53125}, {-1.5 , 0.84 , 2.4},
{-0.84 , 1.5 , 2.4}, {0.0 , 1.5 , 2.4}, {0.784 , 1.4 , 2.4},
{1.4 , 0.784 , 2.4}, {0.749 , 1.3375 , 2.53125}, {1.3375 , 0.749 , 2.53125},
{0.805 , 1.4375 , 2.53125}, {1.4375 , 0.805 , 2.53125}, {0.84 , 1.5 , 2.4},
{1.5 , 0.84 , 2.4}, {1.75 , 0.0 , 1.875}, {1.75 , -0.98 , 1.875},
{0.98 , -1.75 , 1.875}, {0.0 , -1.75 , 1.875}, {2.0 , 0.0 , 1.35},
{2.0 , -1.12 , 1.35}, {1.12 , -2.0 , 1.35}, {0.0 , -2.0 , 1.35},
{2.0 , 0.0 , 0.9}, {2.0 , -1.12 , 0.9}, {1.12 , -2.0 , 0.9},
{0.0 , -2.0 , 0.9}, {-0.98 , -1.75 , 1.875}, {-1.75 , -0.98 , 1.875},
{-1.75 , 0.0 , 1.875}, {-1.12 , -2.0 , 1.35}, {-2.0 , -1.12 , 1.35},
{-2.0 , 0.0 , 1.35}, {-1.12 , -2.0 , 0.9}, {-2.0 , -1.12 , 0.9},
{-2.0 , 0.0 , 0.9}, {-1.75 , 0.98 , 1.875}, {-0.98 , 1.75 , 1.875},
{0.0 , 1.75 , 1.875}, {-2.0 , 1.12 , 1.35}, {-1.12 , 2.0 , 1.35},
{0.0 , 2.0 , 1.35}, {-2.0 , 1.12 , 0.9}, {-1.12 , 2.0 , 0.9},
{0.0 , 2.0 , 0.9}, {0.98 , 1.75 , 1.875}, {1.75 , 0.98 , 1.875},
{1.12 , 2.0 , 1.35}, {2.0 , 1.12 , 1.35}, {1.12 , 2.0 , 0.9},
{2.0 , 1.12 , 0.9}, {2.0 , 0.0 , 0.45}, {2.0 , -1.12 , 0.45},
{1.12 , -2.0 , 0.45}, {0.0 , -2.0 , 0.45}, {1.5 , 0.0 , 0.225},
{1.5 , -0.84 , 0.225}, {0.84 , -1.5 , 0.225}, {0.0 , -1.5 , 0.225},
{1.5 , 0.0 , 0.15}, {1.5 , -0.84 , 0.15}, {0.84 , -1.5 , 0.15},
{0.0 , -1.5 , 0.15}, {-1.12 , -2.0 , 0.45}, {-2.0 , -1.12 , 0.45},
{-2.0 , 0.0 , 0.45}, {-0.84 , -1.5 , 0.225}, {-1.5 , -0.84 , 0.225},
{-1.5 , 0.0 , 0.225}, {-0.84 , -1.5 , 0.15}, {-1.5 , -0.84 , 0.15},
{-1.5 , 0.0 , 0.15}, {-2.0 , 1.12 , 0.45}, {-1.12 , 2.0 , 0.45},
{0.0 , 2.0 , 0.45}, {-1.5 , 0.84 , 0.225}, {-0.84 , 1.5 , 0.225},
{0.0 , 1.5 , 0.225}, {-1.5 , 0.84 , 0.15}, {-0.84 , 1.5 , 0.15},
{0.0 , 1.5 , 0.15}, {1.12 , 2.0 , 0.45}, {2.0 , 1.12 , 0.45},
{0.84 , 1.5 , 0.225}, {1.5 , 0.84 , 0.225}, {0.84 , 1.5 , 0.15},
{1.5 , 0.84 , 0.15}, {-1.6 , 0.0 , 2.025}, {-1.6 , -0.3 , 2.025},
{-1.5 , -0.3 , 2.25}, {-1.5 , 0.0 , 2.25}, {-2.3 , 0.0 , 2.025},
{-2.3 , -0.3 , 2.025}, {-2.5 , -0.3 , 2.25}, {-2.5 , 0.0 , 2.25},
{-2.7 , 0.0 , 2.025}, {-2.7 , -0.3 , 2.025}, {-3.0 , -0.3 , 2.25},
{-3.0 , 0.0 , 2.25}, {-2.7 , 0.0 , 1.8}, {-2.7 , -0.3 , 1.8},
{-3.0 , -0.3 , 1.8}, {-3.0 , 0.0 , 1.8}, {-1.5 , 0.3 , 2.25},
{-1.6 , 0.3 , 2.025}, {-2.5 , 0.3 , 2.25}, {-2.3 , 0.3 , 2.025},
{-3.0 , 0.3 , 2.25}, {-2.7 , 0.3 , 2.025}, {-3.0 , 0.3 , 1.8},
{-2.7 , 0.3 , 1.8}, {-2.7 , 0.0 , 1.575}, {-2.7 , -0.3 , 1.575},
{-3.0 , -0.3 , 1.35}, {-3.0 , 0.0 , 1.35}, {-2.5 , 0.0 , 1.125},
{-2.5 , -0.3 , 1.125}, {-2.65 , -0.3 , 0.9375}, {-2.65 , 0.0 , 0.9375},
{-2.0 , -0.3 , 0.9}, {-1.9 , -0.3 , 0.6}, {-1.9 , 0.0 , 0.6},
{-3.0 , 0.3 , 1.35}, {-2.7 , 0.3 , 1.575}, {-2.65 , 0.3 , 0.9375},
{-2.5 , 0.3 , 1.125}, {-1.9 , 0.3 , 0.6}, {-2.0 , 0.3 , 0.9},
{1.7 , 0.0 , 1.425}, {1.7 , -0.66 , 1.425}, {1.7 , -0.66 , 0.6},
{1.7 , 0.0 , 0.6}, {2.6 , 0.0 , 1.425}, {2.6 , -0.66 , 1.425},
{3.1 , -0.66 , 0.825}, {3.1 , 0.0 , 0.825}, {2.3 , 0.0 , 2.1},
{2.3 , -0.25 , 2.1}, {2.4 , -0.25 , 2.025}, {2.4 , 0.0 , 2.025},
{2.7 , 0.0 , 2.4}, {2.7 , -0.25 , 2.4}, {3.3 , -0.25 , 2.4},
{3.3 , 0.0 , 2.4}, {1.7 , 0.66 , 0.6}, {1.7 , 0.66 , 1.425},
{3.1 , 0.66 , 0.825}, {2.6 , 0.66 , 1.425}, {2.4 , 0.25 , 2.025},
{2.3 , 0.25 , 2.1}, {3.3 , 0.25 , 2.4}, {2.7 , 0.25 , 2.4},
{2.8 , 0.0 , 2.475}, {2.8 , -0.25 , 2.475}, {3.525 , -0.25 , 2.49375},
{3.525 , 0.0 , 2.49375}, {2.9 , 0.0 , 2.475}, {2.9 , -0.15 , 2.475},
{3.45 , -0.15 , 2.5125}, {3.45 , 0.0 , 2.5125}, {2.8 , 0.0 , 2.4},
{2.8 , -0.15 , 2.4}, {3.2 , -0.15 , 2.4}, {3.2 , 0.0 , 2.4},
{3.525 , 0.25 , 2.49375}, {2.8 , 0.25 , 2.475}, {3.45 , 0.15 , 2.5125},
{2.9 , 0.15 , 2.475}, {3.2 , 0.15 , 2.4}, {2.8 , 0.15 , 2.4},
{0.0 , 0.0 , 3.15}, {0.0 , -0.002 , 3.15}, {0.002 , 0.0 , 3.15},
{0.8 , 0.0 , 3.15}, {0.8 , -0.45 , 3.15}, {0.45 , -0.8 , 3.15},
{0.0 , -0.8 , 3.15}, {0.0 , 0.0 , 2.85}, {0.2 , 0.0 , 2.7},
{0.2 , -0.112 , 2.7}, {0.112 , -0.2 , 2.7}, {0.0 , -0.2 , 2.7},
{-0.002 , 0.0 , 3.15}, {-0.45 , -0.8 , 3.15}, {-0.8 , -0.45 , 3.15},
{-0.8 , 0.0 , 3.15}, {-0.112 , -0.2 , 2.7}, {-0.2 , -0.112 , 2.7},
{-0.2 , 0.0 , 2.7}, {0.0 , 0.002 , 3.15}, {-0.8 , 0.45 , 3.15},
{-0.45 , 0.8 , 3.15}, {0.0 , 0.8 , 3.15}, {-0.2 , 0.112 , 2.7},
{-0.112 , 0.2 , 2.7}, {0.0 , 0.2 , 2.7}, {0.45 , 0.8 , 3.15},
{0.8 , 0.45 , 3.15}, {0.112 , 0.2 , 2.7}, {0.2 , 0.112 , 2.7},
{0.4 , 0.0 , 2.55}, {0.4 , -0.224 , 2.55}, {0.224 , -0.4 , 2.55},
{0.0 , -0.4 , 2.55}, {1.3 , 0.0 , 2.55}, {1.3 , -0.728 , 2.55},
{0.728 , -1.3 , 2.55}, {0.0 , -1.3 , 2.55}, {1.3 , 0.0 , 2.4},
{1.3 , -0.728 , 2.4}, {0.728 , -1.3 , 2.4}, {0.0 , -1.3 , 2.4},
{-0.224 , -0.4 , 2.55}, {-0.4 , -0.224 , 2.55}, {-0.4 , 0.0 , 2.55},
{-0.728 , -1.3 , 2.55}, {-1.3 , -0.728 , 2.55}, {-1.3 , 0.0 , 2.55},
{-0.728 , -1.3 , 2.4}, {-1.3 , -0.728 , 2.4}, {-1.3 , 0.0 , 2.4},
{-0.4 , 0.224 , 2.55}, {-0.224 , 0.4 , 2.55}, {0.0 , 0.4 , 2.55},
{-1.3 , 0.728 , 2.55}, {-0.728 , 1.3 , 2.55}, {0.0 , 1.3 , 2.55},
{-1.3 , 0.728 , 2.4}, {-0.728 , 1.3 , 2.4}, {0.0 , 1.3 , 2.4},
{0.224 , 0.4 , 2.55}, {0.4 , 0.224 , 2.55}, {0.728 , 1.3 , 2.55},
{1.3 , 0.728 , 2.55}, {0.728 , 1.3 , 2.4}, {1.3 , 0.728 , 2.4},
{0.0 , 0.0 , 0.0}, {1.5 , 0.0 , 0.15}, {1.5 , 0.84 , 0.15},
{0.84 , 1.5 , 0.15}, {0.0 , 1.5 , 0.15}, {1.5 , 0.0 , 0.075},
{1.5 , 0.84 , 0.075}, {0.84 , 1.5 , 0.075}, {0.0 , 1.5 , 0.075},
{1.425 , 0.0 , 0.0}, {1.425 , 0.798 , 0.0}, {0.798 , 1.425 , 0.0},
{0.0 , 1.425 , 0.0}, {-0.84 , 1.5 , 0.15}, {-1.5 , 0.84 , 0.15},
{-1.5 , 0.0 , 0.15}, {-0.84 , 1.5 , 0.075}, {-1.5 , 0.84 , 0.075},
{-1.5 , 0.0 , 0.075}, {-0.798 , 1.425 , 0.0}, {-1.425 , 0.798 , 0.0},
{-1.425 , 0.0 , 0.0}, {-1.5 , -0.84 , 0.15}, {-0.84 , -1.5 , 0.15},
{0.0 , -1.5 , 0.15}, {-1.5 , -0.84 , 0.075}, {-0.84 , -1.5 , 0.075},
{0.0 , -1.5 , 0.075}, {-1.425 , -0.798 , 0.0}, {-0.798 , -1.425 , 0.0},
{0.0 , -1.425 , 0.0}, {0.84 , -1.5 , 0.15}, {1.5 , -0.84 , 0.15},
{0.84 , -1.5 , 0.075}, {1.5 , -0.84 , 0.075}, {0.798 , -1.425 , 0.0},
{1.425 , -0.798 , 0.0}}; 

// 32 patches each defined by 16 vertices, arranged in a 4 x 4 array 
// NOTE: numbering scheme for teapot has vertices labeled from 1 to 306 
// remnant of the days of FORTRAN 

int indices[32][4][4]={{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}},
{{4, 17, 18, 19}, {8, 20, 21, 22}, {12, 23, 24, 25}, {16, 26, 27, 28}},
{{19, 29, 30, 31}, {22, 32, 33, 34}, {25, 35, 36, 37}, {28, 38, 39, 40}},
{{31, 41, 42, 1}, {34, 43, 44, 5}, {37, 45, 46, 9}, {40, 47, 48, 13}},
{{13, 14, 15, 16}, {49, 50, 51, 52}, {53, 54, 55, 56}, {57, 58, 59, 60}},
{{16, 26, 27, 28}, {52, 61, 62, 63}, {56, 64, 65, 66}, {60, 67, 68, 69}},
{{28, 38, 39, 40}, {63, 70, 71, 72}, {66, 73, 74, 75}, {69, 76, 77, 78}},
{{40, 47, 48, 13}, {72, 79, 80, 49}, {75, 81, 82, 53}, {78, 83, 84, 57}},
{{57, 58, 59, 60}, {85, 86, 87, 88}, {89, 90, 91, 92}, {93, 94, 95, 96}},
{{60, 67, 68, 69}, {88, 97, 98, 99}, {92, 100, 101, 102}, {96, 103, 104, 105}},
{{69, 76, 77, 78}, {99, 106, 107, 108}, {102, 109, 110, 111}, {105, 112, 113, 114}},
{{78, 83, 84, 57}, {108, 115, 116, 85}, {111, 117, 118, 89}, {114, 119, 120, 93}},
{{121, 122, 123, 124}, {125, 126, 127, 128}, {129, 130, 131, 132}, {133, 134, 135, 136}},
{{124, 137, 138, 121}, {128, 139, 140, 125}, {132, 141, 142, 129}, {136, 143, 144, 133}},
{{133, 134, 135, 136}, {145, 146, 147, 148}, {149, 150, 151, 152}, {69, 153, 154, 155}},
{{136, 143, 144, 133}, {148, 156, 157, 145}, {152, 158, 159, 149}, {155, 160, 161, 69}},
{{162, 163, 164, 165}, {166, 167, 168, 169}, {170, 171, 172, 173}, {174, 175, 176, 177}},
{{165, 178, 179, 162}, {169, 180, 181, 166}, {173, 182, 183, 170}, {177, 184, 185, 174}},
{{174, 175, 176, 177}, {186, 187, 188, 189}, {190, 191, 192, 193}, {194, 195, 196, 197}},
{{177, 184, 185, 174}, {189, 198, 199, 186}, {193, 200, 201, 190}, {197, 202, 203, 194}},
{{204, 204, 204, 204}, {207, 208, 209, 210}, {211, 211, 211, 211}, {212, 213, 214, 215}},
{{204, 204, 204, 204}, {210, 217, 218, 219}, {211, 211, 211, 211}, {215, 220, 221, 222}},
{{204, 204, 204, 204}, {219, 224, 225, 226}, {211, 211, 211, 211}, {222, 227, 228, 229}},
{{204, 204, 204, 204}, {226, 230, 231, 207}, {211, 211, 211, 211}, {229, 232, 233, 212}},
{{212, 213, 214, 215}, {234, 235, 236, 237}, {238, 239, 240, 241}, {242, 243, 244, 245}},
{{215, 220, 221, 222}, {237, 246, 247, 248}, {241, 249, 250, 251}, {245, 252, 253, 254}},
{{222, 227, 228, 229}, {248, 255, 256, 257}, {251, 258, 259, 260}, {254, 261, 262, 263}},
{{229, 232, 233, 212}, {257, 264, 265, 234}, {260, 266, 267, 238}, {263, 268, 269, 242}},
{{270, 270, 270, 270}, {279, 280, 281, 282}, {275, 276, 277, 278}, {271, 272, 273, 274}},
{{270, 270, 270, 270}, {282, 289, 290, 291}, {278, 286, 287, 288}, {274, 283, 284, 285}},
{{270, 270, 270, 270}, {291, 298, 299, 300}, {288, 295, 296, 297}, {285, 292, 293, 294}},
{{270, 270, 270, 270}, {300, 305, 306, 279}, {297, 303, 304, 275}, {294, 301, 302, 271}}};

DObject *createTeapot(const PixelColor &objectColor, const Texture *texture, const Texture *bumpmapTexture) {
	DSurfaceObject *obj = new DSurfaceObject();
	
	// b = f_i * u^i * (1-u)^(3-i)
	// b' = f_i * ( i(1-u)^(3-i) * x^(i-1) - (3-i)(1-x)^(2-i) * x^i)
	
	// b = f_i * u^i * (1-u)^(3-i) 
	// b' = f_i * ( i(1-u)^(3-i) * x^(i-1) - (3-i)(1-x)^(2-i) * x^i)
	
	
	// TODO: Bezier curves should be interpolated at varying lengths instead of predetermined lengths.
	//        This would make the part of the curve that is more curved look better.
	
	static const Uint8 N_u = 20;
	static const Uint8 N_v = 20;
	static const Uint8 N_surfaces = 32;
	
	typedef DObject::Vertex Vertex;
	typedef DObject::NormalVector Normal;
	
	for(Uint8 index = 0; index < N_surfaces; ++index) {
	//Uint8 index = 21; {
		Vertex surfaceVertices[N_u+1][N_v+1];
		Normal normals[N_u+1][N_v+1];
		
		for (Uint8 u_i = 0; u_i <= N_u; ++u_i) {
			float u = ((float)u_i) / N_u;
			for (Uint8 v_i = 0; v_i <= N_v; ++v_i) {
				float v = ((float)v_i) / N_v;
				
				Vertex &currentVertex = surfaceVertices[u_i][v_i];
				currentVertex = Vertex(0, 0, 0);
				
				Normal n_du(0, 0, 0);
				Normal n_dv(0, 0, 0);
				
				for(unsigned int i = 0; i < 4; ++i) {
					for(unsigned int j = 0; j < 4; ++j) {
						f3point &f3p = vertices[indices[index][i][j]-1];
						
						Vertex p(f3p[0], f3p[2], f3p[1]);
						p *= (bernstein(i, u) * bernstein(j, v));
						currentVertex += p;
						
						Normal normal_u(f3p[0], f3p[2], f3p[1]);
						//std::cout << "normal_u=" << normal_u << std::endl;
						normal_u *= (bernstein_d(i, u) * bernstein(j, v));
						//std::cout << " normal_u=" << normal_u << std::endl;
						Normal normal_v(f3p[0], f3p[2], f3p[1]);
						normal_v *= (bernstein(i, u) * bernstein_d(j, v));
						n_du += normal_u;
						n_dv += normal_v;
					}
				}
				
				normals[u_i][v_i] = crossProduct(n_du, n_dv);
				//std::cout << "normals["<<(int)u_i<<"]["<<(int)v_i<<"]=" << normals[u_i][v_i] << std::endl;
				
				currentVertex[1] -= 1;
				currentVertex *= 50;
			}
		}
		
		Texture::TexelPosition du = (texture->bottomLeft() - texture->topLeft()) / (N_u + 1);
		Texture::TexelPosition dv = (texture->topRight()   - texture->topLeft()) / (N_v + 1);
		Texture::TexelPosition bdu = (bumpmapTexture->bottomLeft() - bumpmapTexture->topLeft()) / (N_u + 1);
		Texture::TexelPosition bdv = (bumpmapTexture->topRight()   - bumpmapTexture->topLeft()) / (N_v + 1);
		
		for (Uint8 u_i = 0; u_i < N_u; ++u_i) {
			Texture::TexelPosition p(texture->topLeft());
			p += u_i * du;
			Texture::TexelPosition bp(bumpmapTexture->topLeft());
			bp += u_i * bdu;
			for (Uint8 v_i = 0; v_i < N_v; ++v_i, p += dv) {
				obj->addSquareSurface(
					  surfaceVertices[u_i  ][v_i  ]
					, surfaceVertices[u_i  ][v_i+1]
					, surfaceVertices[u_i+1][v_i+1]
					, surfaceVertices[u_i+1][v_i  ]
					, normals[u_i  ][v_i  ]
					, normals[u_i  ][v_i+1]
					, normals[u_i+1][v_i+1]
					, normals[u_i+1][v_i  ]
					, objectColor
					, texture
					, p
					, p + dv
					, p + du + dv
					, p + du
					, bumpmapTexture
					, bp
					, bp + bdv
					, bp + bdu + bdv
					, bp + bdu
				);
			}
		}
	}
	
	return obj;
}
