#ifndef __DSolids_h__
#define __DSolids_h__

#include <SDL/SDL.h>
#include "DObject.h"

class DSolidObject : public DObject {
private:
	DSolidObject();
};

class DCube : public DSolidObject {
public:
	static DObject *create(Uint16 dimensions[3], const PixelColor &objectColor, const Texture *texture = NULL, const Texture *bumpmapTexture = NULL);
};

class DSphere : public DSolidObject {
public:
	static DObject *create(Uint16 radius, Uint8 hlevels, Uint8 vlevels, const PixelColor &objectColor);
};

class DCylinder : public DSolidObject {
public:
	static DObject *create(Uint16 length, Uint16 outerRadius, Uint16 innerRadius, Uint16 circularPoints, const PixelColor &objectColor);
};

class DSpiral : public DSolidObject {
public:
	static DObject *create(Uint16 length, Uint16 radius, Uint16 width, float twists, Uint16 points, const PixelColor &objectColor);
};

DObject *createTeapot(const PixelColor &objectColor, const Texture *texture, const Texture *bumpmapTexture = NULL);

#endif
