#include "DSurface.h"
#include "DObject.h"

#include <cassert>

#define NL  1

DObject::NormalVector createUnitNormal(const DSurface::VertexSet &vs) {
	return createUnitNormal<ObjectCoordinate, 3, VERTICES_PER_SURFACE, NormalCoordinate>(vs);
}

DSurface::DSurface(DSurfaceObject *owner_, const VertexSet &vertices_, const NormalSet &normals_, const Material &material_)
		: N(3)
		, owner(owner_)
		, vertices(vertices_)
		, normals(normals_)
		, material(material_)
		, unitNormal(::createUnitNormal(vertices_)) {
	// TODO: the normals should be stored in normalized length.
	// currently, normalizing them destroys small vectors because of precision errors
	/*
	normals[0] = normals_[0] * NL / normals_[0].length();//.normalized();
	normals[1] = normals_[1] * NL / normals_[1].length();//.normalized();
	normals[2] = normals_[2] * NL / normals_[2].length();//.normalized();
	*/
	/*normals[0] = normals_[0];//.normalized();
	normals[1] = normals_[1];//.normalized();
	normals[2] = normals_[2];//.normalized();*/
}

const DSurface::RenderMode::Mode DSurface::getRenderMode() const { 
	return owner->getRenderMode(); 
}

void DSurface::apply(const Displacement &d) {
	for(unsigned int i = 0; i < N; ++i) {
		d.applyTo(vertices[i]);
		d.applyRotationTo(normals[i]);
	}
	d.applyRotationTo(unitNormal);
}

void DSurface::print(std::ostream &os, unsigned int indent) const {
	ind(os, indent);
	os << "vertices=" << vertices << ", normals=" << normals << std::endl;
}

void DSurface::scale(const float f) {
	vertices *= f;
}

DSurface::NormalVector DSurface::getUnitNormal(Uint8 vertex) const { 
	assert(vertex < N);
	return normals[vertex].normalized(); 
	//return normals[vertex] / NL;
	//return normals[vertex];
}

const DSurface::Vertex DSurface::getCenter() const {
	return ::center(vertices);
}

