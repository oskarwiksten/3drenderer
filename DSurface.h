#ifndef __DSurface_h__
#define __DSurface_h__

#include "Material.h"
#include "MDVector.h"
#include "Rotation.h"

class DSurfaceObject;

typedef float ObjectCoordinate;
typedef float NormalCoordinate;
typedef RotationTranslation<ObjectCoordinate> Displacement;
#define VERTICES_PER_SURFACE 3

class DSurface {
public:
	typedef MDVector<ObjectCoordinate, 3> Vertex;
	typedef MDVector<Vertex, VERTICES_PER_SURFACE> VertexSet;
	typedef MDVector<NormalCoordinate, 3> NormalVector;
	typedef MDVector<NormalVector, VERTICES_PER_SURFACE> NormalSet;

	class RenderMode {
	public:
		enum Mode {
			SURFACE
			,WIREFRAME
		};
	};
	
public:	
	Uint8 N;
	DSurfaceObject *owner;
	VertexSet vertices;
	NormalSet normals;
	Material material;

private:
	NormalVector unitNormal;

public:
	DSurface(DSurfaceObject *owner_, const VertexSet &vertices_, const NormalSet &normals_, const Material &material_);
	
	PixelColor getColor() const { return material.baseColor; }
	const RenderMode::Mode getRenderMode() const;
	void apply(const Displacement &d);
	void scale(const float f);
	void print(std::ostream &os, unsigned int indent = 0) const;

	NormalVector getCenterUnitNormal() const { return unitNormal; }
	NormalVector getUnitNormal(Uint8 vertex) const;
	const Vertex getCenter() const;
};

DSurface::NormalVector createUnitNormal(const DSurface::VertexSet &vs);

#endif
