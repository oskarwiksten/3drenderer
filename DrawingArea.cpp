#include "DrawingArea.h"

#define BPP	4

bool DrawingArea::displayDebugOutput = false;

void DrawingArea::lock() {
	if (surface == NULL) return;
	if (SDL_MUSTLOCK(surface)) {
		SDL_LockSurface(surface);
	}
}

void DrawingArea::unlock() {
	if (surface == NULL) return;
	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface(surface);
	}
}

void DrawingArea::update() {
	if (surface == NULL) return;
	SDL_UpdateRect(surface, 0, 0, 0, 0);
}

void DrawingArea::setSurface(SDL_Surface *surface_) { 
	surface = surface_; 
    if (surface == NULL) return;
	assert(surface->format->BytesPerPixel == BPP);
}

void DrawingArea::putpixel(const PixelPosition &p, const PixelColor color) {
	putpixel(p[0], p[1], color);
}

void DrawingArea::putpixel(const PixelCoordinate x, const PixelCoordinate y, const PixelColor color) {
	if (surface == NULL) return;
	if (x >= surface->clip_rect.w || y >= surface->clip_rect.h) return;
	
	//const int bpp = surface->format->BytesPerPixel;
    Uint8 *p = reinterpret_cast<Uint8*>(surface->pixels) + y * surface->pitch + x * BPP;

    switch(BPP) {
    case 1:
        *p = color;
        break;

    case 2:
        *(Uint16 *)p = color;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (color >> 16) & 0xff;
            p[1] = (color >> 8) & 0xff;
            p[2] = color & 0xff;
        } else {
            p[0] = color & 0xff;
            p[1] = (color >> 8) & 0xff;
            p[2] = (color >> 16) & 0xff;
        }
        break;

    case 4:
        *reinterpret_cast<Uint32*>(p) = color;
        break;
    }
}

void putpixel(SDL_Surface *surface, const PixelPosition &p, const PixelColor color) {
	putpixel(surface, p[0], p[1], color);
}

void putpixel(SDL_Surface *surface, const PixelCoordinate x, const PixelCoordinate y, const PixelColor color) {
	if (surface == NULL) return;
	if (x >= surface->clip_rect.w || y >= surface->clip_rect.h) return;
	
	const int bpp = surface->format->BytesPerPixel;
    Uint8 *p = reinterpret_cast<Uint8*>(surface->pixels) + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        *p = color;
        break;

    case 2:
        *(Uint16 *)p = color;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (color >> 16) & 0xff;
            p[1] = (color >> 8) & 0xff;
            p[2] = color & 0xff;
        } else {
            p[0] = color & 0xff;
            p[1] = (color >> 8) & 0xff;
            p[2] = (color >> 16) & 0xff;
        }
        break;

    case 4:
        *reinterpret_cast<Uint32*>(p) = color;
        break;
    }
	
	//Uint8 *p = reinterpret_cast<Uint8*>(surface->pixels) + y * surface->pitch + x * 4;
	//Uint32 *p = reinterpret_cast<Uint32*>(reinterpret_cast<Uint8*>(surface->pixels) + y * surface->pitch + x * 4);
	//*(static_cast<Uint32*>(surface->pixels) + y * surface->pitch + x * 4) = color;
}

PixelColor getpixel(SDL_Surface *surface_, const PixelPosition &p) {
	return getpixel(surface_, p[0], p[1]);
}

PixelColor getpixel(SDL_Surface *surface_, const PixelCoordinate x, const PixelCoordinate y) {
	if (x >= surface_->clip_rect.w || y >= surface_->clip_rect.h) {
		std::cout << "Warning! Trying to get pixel from " << (int)x << ", " << (int)y << ", which is outside the image!" << std::endl;
	}
	
	const int bpp = surface_->format->BytesPerPixel;
    Uint8 *p = reinterpret_cast<Uint8*>(surface_->pixels) + y * surface_->pitch + x * bpp;

	#warning This is REALLY not optimised!
    switch(bpp) {
    case 1:
		return ((*p) << 16) | ((*p) << 8) | ((*p) << 0);
    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            return (
					  (p[0] <<  0)
					| (p[1] <<  8)
					| (p[2] << 16)
				);
        } else {
            return (
					  (p[0] << 16) 
					| (p[1] <<  8)
					| (p[2] <<  0)
				);
        }
    case 4:
        return *reinterpret_cast<Uint32 *>(p);
    }
	return 0;
}

#define sl(i)  for(std::list<ScanlineListener*>::iterator i = scanlineListeners.begin(); i != scanlineListeners.end(); ++i) 

void DrawingArea::putpixel(const PixelPosition &p) {
	PixelColor color;
	if (colorEvaluator->getPixelColor(p, color)) {
		putpixel(p, color);
	}
}

void beginScanline(ScanlineListener &scanlineListener, const Uint8 beginIndex, const Uint8 endIndex, const Uint16 numSteps_) { 
	scanlineListener.beginSurfaceInterpolation(true, beginIndex, endIndex, 1);
	scanlineListener.beginSurfaceInterpolation(false, beginIndex, endIndex, 1);
	scanlineListener.beginScanline(numSteps_);
}

const Uint8 choose(const PixelPositionTriangle &t, const PixelPosition *p) {
	if (p == &(t[0])) return 0;
	else if (p == &(t[1])) return 1;
	else return 2;
}
void beginSurfaceInterpolation(
		ScanlineListener &scanlineListener, const bool left_, const PixelPositionTriangle &t
		, const PixelPosition *begin, const PixelPosition *end, const Uint16 numSteps_) {
	scanlineListener.beginSurfaceInterpolation(left_, choose(t, begin), choose(t, end), numSteps_);
}

void DrawingArea::drawLine(const PixelPosition &p0, const PixelPosition &p1) {
	const PixelCoordinateDistance xdiff = DIFF(p1[0], p0[0]);
	const PixelCoordinateDistance ydiff = DIFF(p1[1], p0[1]);
	if (xdiff == 0 && ydiff == 0) {
		// just one pixel
		sl(i) { beginScanline(**i, 0, 0, 1); }
		lock();
		putpixel(p0);
		unlock();	
	} else {
		PixelCoordinate height;
		PixelCoordinate miny, maxy;
		if (p0[1] < p1[1]) {
			miny = p0[1];
			maxy = p1[1];
			height = ydiff;
		} else {
			miny = p1[1];
			maxy = p0[1];
			height = -ydiff;
		}
		PixelCoordinate width;
		PixelCoordinate minx, maxx;
		if (p0[0] < p1[0]) {
			minx = p0[0];
			maxx = p1[0];
			width = xdiff;
		} else {
			minx = p1[0];
			maxx = p0[0];
			width = -xdiff;
		}
			
		PixelPosition pixel(p0);
		
		//scanlineListener.beginLine(&p0, &p1);
		
		
		lock();
		if (xdiff == 0) {
			// Special case, vertical line
			sl(j) { beginScanline(**j, 0, 1, maxy-miny); }
			for (pixel[1] = miny; pixel[1] <= maxy; pixel[1]++) {
				//scanlineListener.setLineStep(pixel, pixel[1], miny, maxy);
				putpixel(pixel);
				sl(j) { (*j)->stepScanline(); }
			}
		} else if (ydiff == 0) {
			// Special case, horizontal line
			sl(j) { beginScanline(**j, 0, 1, maxx-minx); }
			for (pixel[0] = minx; pixel[0] <= maxx; pixel[0]++) {
				//scanlineListener.setLineStep(pixel, pixel[0], minx, maxx);
				putpixel(pixel);
				sl(j) { (*j)->stepScanline(); }
			}
		} else {
			PixelCoordinate i;
			if (height > width) {
				// Iterate one pixel vertically
				sl(j) { beginScanline(**j, 0, 1, height); }
				for (i = 0; i <= height; i++) {
					pixel[0] = p0[0] + static_cast<PixelCoordinateDistance>(i) * xdiff / static_cast<PixelCoordinateDistance>(height);
					//scanlineListener.setLineStep(pixel, i, 0, height);
					putpixel(pixel);
					if (ydiff > 0) pixel[1]++;
					else pixel[1]--;
					sl(j) { (*j)->stepScanline(); }
				}
			} else {
				// Iterate one pixel horizontally
				sl(j) { beginScanline(**j, 0, 1, width); }
				for (i = 0; i <= width; i++) {
					pixel[1] = p0[1] + static_cast<PixelCoordinateDistance>(i) * ydiff / static_cast<PixelCoordinateDistance>(width);
					//scanlineListener.setLineStep(pixel, i, 0, width);
					putpixel(pixel);
					if (xdiff > 0) pixel[0]++;
					else pixel[0]--;
					sl(j) { (*j)->stepScanline();}
				}
			}
		}
		sl(i) { (*i)->endScanline(); }
		
		unlock();
	}
}


// Sets the leftmost point of p0, p1 into "left" and the rightmost into "right"
inline void setLeftRight(
		const PixelPosition &p0
		,const PixelPosition &p1
		,const PixelPosition **left
		,const PixelPosition **right) {
	if (p0[0] < p1[0]) {
		*left = &p0; 
		*right = &p1;
	} else {
		*left = &p1; 
		*right = &p0;
	}
}

/*
void fillTriangle2(SDL_Surface *surface, refPixelPositionTriangle t, Uint8 r, Uint8 g, Uint8 b, bool update) {
	Sint16 x[3] = {t[0][0], t[1][0], t[2][0]};
	Sint16 y[3] = {t[0][1], t[1][1], t[2][1]};
	filledPolygonRGBA(surface, x, y, 3, r, g, b, 255);
	//if (update) updatesdl(surface, minx, (*top)[1], width, height);
}
*/

template<class T>
void swap(T &a, T &b) {
	const T temp = b;
	b = a;
	a = temp;
}

void DrawingArea::fillTriangle(const PixelPositionTriangle &t) {
	const PixelPosition *top; // topmost vertex
	const PixelPosition *left1; // current bottom vertex on the left side
	const PixelPosition *right1; // current bottom vertex on the right side
	
	// Find topmost vertex
	if (t[0][1] <= t[1][1] && t[0][1] <= t[2][1]) {
		top = &(t[0]);
		setLeftRight(t[1], t[2], &left1, &right1);
	} else if (t[1][1] <= t[0][1] && t[1][1] <= t[2][1]) {
		top = &(t[1]);
		setLeftRight(t[0], t[2], &left1, &right1);
	} else {
		top = &(t[2]);
		setLeftRight(t[0], t[1], &left1, &right1);
	}
	const PixelPosition *left0 = top; // current top vertex on the left side
	const PixelPosition *right0 = top; // current top vertex on the right side
	
	const PixelCoordinate maxy = MAX((*left1)[1], (*right1)[1]);
	
	PixelCoordinateDistance left_dx = DIFF((*left1)[0], (*left0)[0]);
	PixelCoordinateDistance left_dy = DIFF((*left1)[1], (*left0)[1]);
	PixelCoordinateDistance right_dx = DIFF((*right1)[0], (*right0)[0]);
	PixelCoordinateDistance right_dy = DIFF((*right1)[1], (*right0)[1]);
	
	// if (left_dx / left_dy > right_dx / right_dy)
	// if this is the case, then 
	if (left_dx * right_dy > left_dy * right_dx) {
		// left1 is more to the left than right1, 
		// BUT right1 is above left1, like this:
		//       left0 ->       ___...+  <- right0
		//   right1 ->     +'''  __.-'
		//                / _..-'
		// left1 ->      +''
		// so we swap left and right.
		swap(left1, right1);
		swap(left_dx, right_dx);
		swap(left_dy, right_dy);
	}
	
	PixelPosition pixel(*top);
	
	lock();
	
	sl(i) { 
		beginSurfaceInterpolation(**i, true , t, left0 , left1 , left_dy );
		beginSurfaceInterpolation(**i, false, t, right0, right1, right_dy);
	}
	
	for (; pixel[1] <= maxy; pixel[1]++) {
		
		if (pixel[1] == (*left1)[1]) {
			// we have reached the left vertex.
			left0 = left1;
			left1 = right1;
			left_dx = DIFF((*left1)[0], (*left0)[0]);
			left_dy = DIFF((*left1)[1], (*left0)[1]);
			sl(i) { beginSurfaceInterpolation(**i, true , t, left0, left1, left_dy); }
		} 
		if (pixel[1] == (*right1)[1]) {
			// we have reached the right vertex.
			right0 = right1;
			right1 = left1;
			right_dx = DIFF((*right1)[0], (*right0)[0]);
			right_dy = DIFF((*right1)[1], (*right0)[1]);
			sl(i) { beginSurfaceInterpolation(**i, false, t, right0, right1, right_dy); }
		}		
		
		PixelCoordinate leftx = (*left0)[0];
		PixelCoordinate rightx = (*right0)[0];
		if (left_dy != 0) {
			leftx += static_cast<PixelCoordinate>(static_cast<float>(left_dx) * DIFF(pixel[1], (*left0)[1]) / left_dy); 
		}
		if (right_dy != 0) {
			rightx += static_cast<PixelCoordinate>(static_cast<float>(right_dx) * DIFF(pixel[1], (*right0)[1]) / right_dy); 
		}
		
		//assert(leftx <= rightx);
		
		sl(i) { (*i)->beginScanline(rightx - leftx); }
		
		//std::cout << "y[" << ((int) pixel[1]) << "] : (" << ((int) leftx) << ", " << ((int) rightx) << ")" << std::endl;
		for (pixel[0] = leftx; pixel[0] <= rightx; pixel[0]++) {
			putpixel(pixel);
			sl(i) { (*i)->stepScanline(); }
		}
		sl(i) { (*i)->endScanline(); }
	}
		
	unlock();
}

void DrawingArea::drawTriangle(const PixelPositionTriangle &t) {
	drawLine(t[0], t[1]);
	drawLine(t[1], t[2]);
	drawLine(t[2], t[0]);
}

