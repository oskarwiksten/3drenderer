#ifndef __DrawingArea_h__
#define __DrawingArea_h__

#include <SDL/SDL.h>
#include "MDVector.h"
#include <list>

typedef Uint32 PixelCoordinate;
typedef Sint32 PixelCoordinateDistance;
typedef Uint32 PixelColor;
typedef MDVector<PixelCoordinate, 2> PixelPosition;
typedef MDVector<PixelPosition, 3> PixelPositionTriangle;

class PixelColorEvaluator {
public:
	virtual ~PixelColorEvaluator() {}
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color) = 0;
};

class ScanlineListener {
public:
	virtual ~ScanlineListener() {}
	virtual void beginSurfaceInterpolation(const bool left, const Uint8 beginIndex, const Uint8 endIndex, const Uint16 numSteps_) = 0;
	virtual void beginScanline(const Uint16 numSteps_) = 0;
	virtual void endScanline() = 0;
	virtual void stepScanline() = 0;
};

class DrawingArea {
public:
	static bool displayDebugOutput;

private:
	SDL_Surface *surface;
	PixelColorEvaluator *colorEvaluator;
	std::list<ScanlineListener *> scanlineListeners;

	void lock();
	void unlock();
	void putpixel(const PixelPosition &p);

public:
	void putpixel(const PixelPosition &p, PixelColor color);
	void putpixel(const PixelCoordinate x, const PixelCoordinate y, const PixelColor color);

	void setSurface(SDL_Surface *surface_);
	void setPixelColorEvaluator(PixelColorEvaluator &colorEvaluator_) { colorEvaluator = &colorEvaluator_; }
	void clearScanlineListeners() { scanlineListeners.clear(); }
	void addScanlineListener(ScanlineListener &scanlineListener) {
		scanlineListeners.push_back(&scanlineListener);
	}

	void drawLine(const PixelPosition &p0, const PixelPosition &p1);
	void drawTriangle(const PixelPositionTriangle &t);
	void fillTriangle(const PixelPositionTriangle &t);
	
	void update();
};

PixelColor getpixel(SDL_Surface *surface_, const PixelPosition &p);
PixelColor getpixel(SDL_Surface *surface_, const PixelCoordinate x, const PixelCoordinate y);
void putpixel(SDL_Surface *surface, const PixelPosition &p, const PixelColor color);
void putpixel(SDL_Surface *surface, const PixelCoordinate x, const PixelCoordinate y, const PixelColor color);


#endif
