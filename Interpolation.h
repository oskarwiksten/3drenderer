#ifndef __Interpolation_h__
#define __Interpolation_h__

template<class InterpolationType>
class Interpolation {
private:
	InterpolationType begin;
	InterpolationType end;
	Uint16 currentStep;
	Uint16 numSteps;

	//InterpolationType perStep;
	//InterpolationType current;

public:
	Interpolation() {}
	void initialize(const InterpolationType &begin_, const InterpolationType &end_, const Uint16 numSteps_) {
		begin = begin_;
		end = end_;
		currentStep = 0;
		numSteps = numSteps_;
		
		//current = begin_;
		//perStep = (end_ - begin_) / (numSteps_ + 1);
	}
	
	void step() { 
		//current += perStep; 
		currentStep++;
	}
	const InterpolationType getInterpolatedValue() const {
		//return current;
		if (currentStep == 0) return begin;
		else if (currentStep >= numSteps) return end;
		else {
			//float v = static_cast<float>(currentStep - beginValue) / valueLength;
			float v = static_cast<float>(currentStep) / numSteps;
			//if (v < 0) return begin;
			//else if (v > 1.0f) return end;
			/*else*/ return end * v + begin * (1.0f-v);
		}
	}
};

template<class InterpolationType>
class SurfaceInterpolation : public ScanlineListener {
private:
	Interpolation<InterpolationType> left;
	Interpolation<InterpolationType> right;
	Interpolation<InterpolationType> current;
public:
	virtual ~SurfaceInterpolation() {}

	void beginSurfaceInterpolation(
			const bool left_
			,const InterpolationType &begin, const InterpolationType &end
			,const Uint16 numSteps_) {
		if (left_) {
			left.initialize(begin, end, numSteps_);
		} else {
			right.initialize(begin, end, numSteps_);
		}
	}
	
	virtual void beginScanline(const Uint16 numSteps_) { 
		current.initialize(left.getInterpolatedValue(), right.getInterpolatedValue(), numSteps_);
	}
	virtual void endScanline() { left.step(); right.step(); }
	virtual void stepScanline() { current.step();}
	const InterpolationType getInterpolatedValue() const { return current.getInterpolatedValue(); }
};

template<class InterpolationType>
class TriangularInterpolation : public SurfaceInterpolation<InterpolationType> {
private:
	InterpolationType p[3];
public:
	virtual ~TriangularInterpolation() {}

	void initialize(const InterpolationType &p0_, const InterpolationType &p1_, const InterpolationType &p2_) {
		p[0] = p0_;
		p[1] = p1_;
		p[2] = p2_;
	}
	
	virtual void beginSurfaceInterpolation(const bool left_, const Uint8 beginIndex, const Uint8 endIndex, const Uint16 numSteps_) {
		SurfaceInterpolation<InterpolationType>::beginSurfaceInterpolation(left_, p[beginIndex], p[endIndex], numSteps_);
	}
};

#endif
