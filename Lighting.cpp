#include "Lighting.h"
#include <SDL/SDL.h>

static const float maxtheta = 1.7f;

void Lighting::initialize(SDL_PixelFormat *pixelFormat_, const DSurface::Vertex &eye_, const PixelPosition &center_) {
	pixelFormat = pixelFormat_;
	eye = eye_;
	center = center_;
}

PixelColor Lighting::getShadingColor(float theta, const Uint8 r, const Uint8 g, const Uint8 b) const {
	if (theta > maxtheta) {
		theta = maxtheta;
	}

	float f = 1 - theta / maxtheta;
	return SDL_MapRGB(pixelFormat
			, static_cast<Uint8>(r * f)
			, static_cast<Uint8>(g * f)
			, static_cast<Uint8>(b * f));
}


void SolidColorLighting::setCurrentSurface(const DSurface &surface) {
	setColor(surface.getColor());
}

bool SolidColorLighting::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	color_ = color;
	return true;
}

void SolidColorLighting::setColor(const PixelColor &color_) {
	color = color_;
}




void SimpleZLighting::setCurrentSurface(const DSurface &surface) {
	const DSurface::NormalVector surfaceNormal = surface.getCenterUnitNormal();
	//cos(v) = (viewpoint (dot) normal) / (|viewpoint| * |normal|)
	float theta = MathHelper::acos(surfaceNormal[2]);
	
	Uint8 r, g, b;
	SDL_GetRGB(surface.getColor(), pixelFormat, &r, &g, &b);
	color = getShadingColor(theta, r, g, b);
}

bool SimpleZLighting::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	color_ = color;
	return true;
}


void ViewpointAngleLighting::setCurrentSurface(const DSurface &surface) {
	const DSurface::Vertex center = surface.getCenter();
	const DSurface::Vertex viewpoint(eye - center);
	const DSurface::NormalVector surfaceNormal = surface.getCenterUnitNormal();

	//cos(v) = (viewpoint (dot) normal) / (|viewpoint| * |normal|)
	const float theta = MathHelper::acos(surfaceNormal.dotProduct(viewpoint) / viewpoint.length());
	
	Uint8 r, g, b;
	SDL_GetRGB(surface.getColor(), pixelFormat, &r, &g, &b);
	color = getShadingColor(theta, r, g, b);
}

bool ViewpointAngleLighting::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	color_ = color;
	return true;
}



void PixelAngleLighting::setCurrentSurface(const DSurface &surface) {
	const DSurface::Vertex center = surface.getCenter();
	const DSurface::Vertex viewpoint(eye - center);
	surfaceNormal = surface.getCenterUnitNormal();
	SDL_GetRGB(surface.getColor(), pixelFormat, &r, &g, &b);
}

bool PixelAngleLighting::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	const DSurface::Vertex pixel(
		  ((Sint32) p[0] - (Sint32) center[0])
		, -((Sint32) p[1] - (Sint32) center[1])
		, 0);
	const DSurface::Vertex viewpoint(eye - pixel);
	
	float theta = MathHelper::acos(getSurfaceNormal().dotProduct(viewpoint) / viewpoint.length());
	
	color_ = getShadingColor(theta, r, g, b);
	return true;
}



void InterpolatedNormalLighting::setCurrentSurface(const DSurface &surface) {
	PixelAngleLighting::setCurrentSurface(surface);
	currentSurface = &surface;
	normalInterpolation.initialize(currentSurface->normals[0], currentSurface->normals[1], currentSurface->normals[2]);
}

const DSurface::NormalVector InterpolatedNormalLighting::getSurfaceNormal() const { 
	//TODO: This normalization should be unneccessary if the surface normals were
	//       pre-normalized.
	return normalInterpolation.getInterpolatedValue().normalized();
}



void ZBufferEvaluator::setLighting(Lighting *lighting_) { 
	lighting = lighting_; 
}
void ZBufferEvaluator::setBufferSize(const PixelCoordinate width_, const PixelCoordinate height_) {
	width = width_;
	height = height_;
	std::cout << "Resizing buffer. zbuffer is now " << (width_ * height_ * sizeof(ZValue) / (1024)) << "kb." << std::endl;
	buffer.resize(width_ * height_);
	clearBuffer();
}

static const Sint16 UNDEFINED = -999;

void ZBufferEvaluator::clearBuffer() {
	std::fill(buffer.begin(), buffer.end(), UNDEFINED);
}

void ZBufferEvaluator::setCurrentSurface(const DSurface &surface) {
	currentSurface = &surface;
	lighting->setCurrentSurface(surface);
	zInterpolation.initialize(currentSurface->vertices[0][2], currentSurface->vertices[1][2], currentSurface->vertices[2][2]);
}

bool ZBufferEvaluator::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	const Index i = CoordToIndex(p);
	const ZValue z = zInterpolation.getInterpolatedValue();
	if (z > buffer[i] || UNDEFINED == buffer[i]) {
		buffer[i] = z;
	} else {
		return false;
	}
	
	return lighting->getPixelColor(p, color_);
}



/*static const float c_diffuse = 0.5f;
static const float c_specular = 0.3f;
static const Uint8 c_specular_shininess = 5;
static const float c_ambient = 0.2f;
*/
//static const float c_attenuation = 1.0f;

//Ix = AxKaDx + AttLx [KdDx(N dot L) + KsSx(R dot V)^n]

bool PhongLighting::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	const DSurface::Vertex pixel(
		  ((Sint32) p[0] - (Sint32) center[0])
		, -((Sint32) p[1] - (Sint32) center[1])
		, 0);
	const Material &material = currentSurface->material;
	DSurface::NormalVector viewpoint(eye - pixel);
	viewpoint.normalize();
	const DSurface::NormalVector light(viewpoint);
	const DSurface::NormalVector surfaceNormal = getSurfaceNormal();
	ObjectCoordinate NdotL = light.dotProduct(surfaceNormal);
	if (NdotL < 0) NdotL = 0;
	const DSurface::NormalVector reflectedSpecular(2 * surfaceNormal * NdotL - light);
	
	typedef Uint16 ColorValue;
	typedef MDVector<ColorValue, 3> ColorVector;
	static const ColorValue cm = 255;
	const ColorVector color_Material(r, g, b);
	static const ColorVector white(cm, cm, cm);
	static const ColorVector color_Specular(white);
	//static const ColorVector color_Light(white);
	//static const ColorVector color_Ambient(white);
	
	ColorVector intensity(0, 0, 0);
	// Ambient
	intensity += /*color_Ambient * */material.c_ambient * color_Material;
	// Diffuse
	intensity += /*c_attenuation * color_Light * */material.c_diffuse * color_Material * NdotL;
	
	if (reflectedSpecular[2] > 0) {
		const ObjectCoordinate RdotV = reflectedSpecular.dotProduct(viewpoint);
		// Specular
		intensity += /*c_attenuation * color_Light * */material.c_specular * color_Specular * powf(RdotV, material.c_specular_shininess);
	}
	
	for(Uint8 i = 0; i <= 2; ++i) {
		if (intensity[i] > cm) intensity[i] = cm;
		else if (intensity[i] < 0) intensity[i] = 0;
	}
	
	color_ = SDL_MapRGB(pixelFormat
		, static_cast<Uint8>(intensity[0])
		, static_cast<Uint8>(intensity[1])
		, static_cast<Uint8>(intensity[2])
	);
	
	return true;
}

void TextureLighting::setCurrentSurface(const DSurface &surface) {
	PhongLighting::setCurrentSurface(surface);
	const Material &material = surface.material;

	textureInterpolation.initialize(material.texturePositions[0], material.texturePositions[1], material.texturePositions[2]);
	texture = material.texture;
	
	bumpmapTextureInterpolation.initialize(material.bumpmapTexturePositions[0], material.bumpmapTexturePositions[1], material.bumpmapTexturePositions[2]);
	bumpmapTexture = material.bumpmapTexture;
	
}

bool TextureLighting::getPixelColor(const PixelPosition &p, PixelColor &color_) {
	if (texture != NULL) {
		PixelColor baseColor = getpixel(texture->image, textureInterpolation.getInterpolatedValue());
		SDL_GetRGB(baseColor, pixelFormat, &r, &g, &b);
	}
	return PhongLighting::getPixelColor(p, color_);
}

const DSurface::NormalVector TextureLighting::getSurfaceNormal() const {
	DSurface::NormalVector v = PhongLighting::getSurfaceNormal();
	if (bumpmapTexture != NULL) {
		Uint8 r, g, b;
		PixelColor baseColor = getpixel(bumpmapTexture->image, bumpmapTextureInterpolation.getInterpolatedValue());
		SDL_GetRGB(baseColor, pixelFormat, &r, &g, &b);
		
		const NormalCoordinate dx = (static_cast<NormalCoordinate>(r) - 127.0f) / 128.0f;
		const NormalCoordinate dy = (static_cast<NormalCoordinate>(g) - 127.0f) / 128.0f;
		
		v += (currentSurface->vertices[2] - currentSurface->vertices[0]).normalized() * dx * 0.5f;
		v += (currentSurface->vertices[1] - currentSurface->vertices[0]).normalized() * dy * 0.5f;
	}
	return v;
}

