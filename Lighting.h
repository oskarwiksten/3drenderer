#ifndef __Lighting_h__
#define __Lighting_h__

#include "DrawingArea.h"
#include "DObject.h"

class Lighting : public PixelColorEvaluator {
protected:
	SDL_PixelFormat *pixelFormat;
	DSurface::Vertex eye;
	PixelPosition center;

	PixelColor getShadingColor(float theta, const Uint8 r, const Uint8 g, const Uint8 b) const;

public:
	virtual ~Lighting() {}
	void initialize(SDL_PixelFormat *pixelFormat_, const DSurface::Vertex &eye_, const PixelPosition &center_);
	virtual void setCurrentSurface(const DSurface &surface) = 0;
	virtual void addScanLineListeners(DrawingArea &dest) { }
};


class SolidColorLighting : public Lighting {
private:
	PixelColor color;
public:
	virtual ~SolidColorLighting() {}
	virtual void setCurrentSurface(const DSurface &surface);
	void setColor(const PixelColor &color_);
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
};


class SimpleZLighting : public Lighting {
private:
	PixelColor color;
public:
	virtual ~SimpleZLighting() {}
	virtual void setCurrentSurface(const DSurface &surface);
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
};


class ViewpointAngleLighting : public Lighting {
private:
	PixelColor color;
public:
	virtual ~ViewpointAngleLighting() {}
	virtual void setCurrentSurface(const DSurface &surface);
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
};


class PixelAngleLighting : public Lighting {
private:
	DSurface::NormalVector surfaceNormal;
protected:
	Uint8 r, g, b;
	virtual const DSurface::NormalVector getSurfaceNormal() const { return surfaceNormal; }
public:
	virtual ~PixelAngleLighting() {}
	virtual void setCurrentSurface(const DSurface &surface);
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
};

#include "Interpolation.h"

class InterpolatedNormalLighting : public PixelAngleLighting {
private:
	TriangularInterpolation<DSurface::NormalVector> normalInterpolation;
protected:
	const DSurface *currentSurface;
	virtual const DSurface::NormalVector getSurfaceNormal() const;

public:
	virtual ~InterpolatedNormalLighting() { }
	virtual void setCurrentSurface(const DSurface &surface);
	virtual void addScanLineListeners(DrawingArea &dest) {
		dest.addScanlineListener(normalInterpolation);
	}
};

class ZBufferEvaluator : public Lighting {
private:
	Lighting *lighting;
	const DSurface *currentSurface;
	
	typedef Uint32 Index;
	typedef Sint16 ZValue;

	PixelCoordinate width;
	PixelCoordinate height;
	std::vector<ZValue> buffer;

	TriangularInterpolation<ZValue> zInterpolation;

	Index CoordToIndex(const PixelCoordinate &p_x, const PixelCoordinate &p_y) const {
		return width * p_y + p_x;
	}
	Index CoordToIndex(const PixelPosition &p) const { return CoordToIndex(p[0], p[1]); }
	//PixelPosition IndexToCoord(const Index &i) const { return PixelPosition(i % width, i / width); }
	
public:

	void clearBuffer();
	void setLighting(Lighting *lighting_);
	void setBufferSize(const PixelCoordinate width_, const PixelCoordinate height_);
	
	virtual ~ZBufferEvaluator() { }
	virtual void setCurrentSurface(const DSurface &surface);
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
	virtual void addScanLineListeners(DrawingArea &dest) {
		dest.addScanlineListener(zInterpolation);
	}
	
};

class PhongLighting : public InterpolatedNormalLighting {
public:
	virtual ~PhongLighting() {}
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
};

class TextureLighting : public PhongLighting {
private:
	//TriangularInterpolation<DObject::Vertex> textureInterpolation;
	TriangularInterpolation<Texture::TexelPosition> textureInterpolation;
	TriangularInterpolation<Texture::TexelPosition> bumpmapTextureInterpolation;
protected:
	const Texture *texture;
	const Texture *bumpmapTexture;
	virtual const DSurface::NormalVector getSurfaceNormal() const;

public:
	virtual ~TextureLighting() { }
	virtual void addScanLineListeners(DrawingArea &dest) {
		dest.addScanlineListener(textureInterpolation);
		dest.addScanlineListener(bumpmapTextureInterpolation);
		PhongLighting::addScanLineListeners(dest);
	}
	virtual void setCurrentSurface(const DSurface &surface);
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_);
};

#endif
