#ifndef __MDTriangle_h__
#define __MDTriangle_h__

#include <iostream>
#include "MDVector.h"

/*
// TODO: The template argument should be the vertex type, so that the triangle
//        won't neccessarily be bound to using a MDVector
template <class T, unsigned int D> 
class MDTriangle {
public:
	typedef MDVector<T, D> Vertex;
	typedef MDVector<T, D> Direction;
private:
	Vertex p[3];
public:
	MDTriangle();
	MDTriangle(const Vertex &p0_, const Vertex &p1_, const Vertex &p2_);
	
	void set(const Vertex &p0_, const Vertex &p1_, const Vertex &p2_);
	Vertex &operator[](const Uint8 i) { return p[i]; }
	const Vertex &operator[](const Uint8 i) const { return p[i]; }
	
	template <class T_, unsigned int D_>
	friend std::ostream &operator<< (std::ostream &os, const MDTriangle<T_, D_> &p);
	
	Vertex center() const;
	Direction normal() const;

	template <class Precision> 
	MDVector<Precision, D> unitNormal() const;
	
	template <class T_>
	void operator*=(const T_ v) { p[0] *= v; p[1] *= v; p[2] *= v; }
};
*/

/*
template <class T, class Precision>
bool intersection(const MDTriangle<T, 3> &plane, const MDVector<T, 3> &origin, const MDVector<T, 3> &ray, Precision &u, Precision &v);
*/
	
	
#include "MDTriangle.hpp"

#endif
