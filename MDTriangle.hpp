
/*
template <class T, unsigned int D>
MDTriangle<T, D>::MDTriangle() {
}

template <class T, unsigned int D>
MDTriangle<T, D>::MDTriangle(const Vertex &p0_, const Vertex &p1_, const Vertex &p2_) {
	set(p0_, p1_, p2_);
}

template <class T, unsigned int D>
void MDTriangle<T, D>::set(const Vertex &p0_, const Vertex &p1_, const Vertex &p2_) {
	p[0] = p0_;
	p[1] = p1_;
	p[2] = p2_;
}

template <class T, unsigned int D>  
std::ostream &operator<<(std::ostream &os, const MDTriangle<T, D> &t) {
	return os << '{' << t[0] << ", " << t[1] << ", " << t[2] << '}';
}

template <class T, unsigned int D>
MDVector<T, D> MDTriangle<T, D>::center() const {
	Vertex c(p[0]);
	c += p[1];
	c += p[2];
	c /= 3;
	return c;
}

template <class T, unsigned int D>
MDVector<T, D> MDTriangle<T, D>::normal() const {
	// 
	//                 p0
	//                 .
	//         __..--'' '-.
	//  p2  ----------------  p1
	return crossProduct((p[0] - p[1]), (p[2] - p[1]));
}

template <class T, unsigned int D>
template <class Precision>
MDVector<Precision, D> MDTriangle<T, D>::unitNormal() const {
	const MDVector<Precision, D> a(p[0] - p[1]);
	const MDVector<Precision, D> b(p[2] - p[1]);
	return crossProduct(a, b).normalized();
}
*/




/*
template <class T, class Precision>
bool intersection(const MDTriangle<T, 3> &plane, const MDVector<T, 3> &origin, const MDVector<T, 3> &ray, Precision &u, Precision &v) {
	const MDVector<Precision, 3> normalizedray = ray.normalized();
	
	const MDVector<Precision, 3> edge1 = plane.p1 - plane.p0;
	const MDVector<Precision, 3> edge2 = plane.p2 - plane.p0;
	
	const MDVector<Precision, 3> pvec = crossProduct(normalizedray, edge2);
	
	const Precision a = pvec.dotProduct(edge1);
	if (a < 1e-6) return false;
	const Precision f = static_cast<Precision>(1) / a;

	const MDVector<Precision, 3> tvec = origin - plane.p0;
	
	u = f * pvec.dotProduct(tvec);
	//if (u < 0 || u > 1.0f) return false;
		
	const MDVector<Precision, 3> qvec = crossProduct(tvec, edge1);
	v = f * qvec.dotProduct(normalizedray);
	//if (v < 0 || (u+v) > 1.0f) return false;

	return true;
}
*/

/*
template <class T, unsigned int D>
void MDTriangle<T, D>::translate(const Direction &offset) {
	if (!offset) return;
	p0 += offset;
	p1 += offset;
	p2 += offset;
}

template <class T, unsigned int D>
void MDTriangle<T, D>::rotate(const Rotation &direction) {
	if (!direction) return;
	p0.rotate(direction);
	p1.rotate(direction);
	p2.rotate(direction);
}
*/
