#ifndef __MDVector_h__
#define __MDVector_h__

#include <iostream>

template <class T, unsigned int D>
class MDVector {
private:
	T v[D];
	//bool isNormalized;

public:
	MDVector();
	template<class T2>
	explicit MDVector(T2 x);
	template<class T2, class T3>
	explicit MDVector(T2 x, T3 y);
	template<class T2, class T3, class T4>
	explicit MDVector(T2 x, T3 y, T4 z);
	template<class T2, class T3, class T4, class T5>
	explicit MDVector(T2 a, T3 b, T4 c, T5 d);
	template <class T2>
	MDVector(const T2 v_[D]);
	template <class T2>
	MDVector(const MDVector<T2,D> &p);
	
	static const MDVector<T,D> zero;
	 
	template <class T2, unsigned int D2>
	friend std::ostream &operator<< (std::ostream &os, const MDVector<T2,D2> &p);
	
	template <class T2>
	MDVector<T,D> &operator=(const MDVector<T2,D> &p);
	template <class T2>
	MDVector<T,D> &operator=(const T2 v_[D]);
	template <class T2>
	bool operator==(const MDVector<T2,D> &p) const;
	template <class T2>
	bool operator==(const T2 v_[D]) const;
	template <class T2>
	bool operator!=(const MDVector<T2,D> &p) const;
	template <class T2>
	bool operator!=(const T2 v_[D]) const;
	inline T &operator[](unsigned int d);
	inline const T &operator[](unsigned int d) const;
	
	void opposite();
	MDVector<T,D> getOpposite() const;
	
	bool operator!() const;
	MDVector<T,D> &operator++();
	template<class V>
	MDVector<T,D> operator+(V val) const;
	template <class T2>
	MDVector<T,D> operator+(const MDVector<T2,D> &p) const;
	template<class V>
	MDVector<T,D> &operator+=(V val);
	template <class T2>
	MDVector<T,D> &operator+=(const MDVector<T2,D> &p);
	
	MDVector<T,D> operator-() const;
	MDVector<T,D> &operator--();
	template<class V>
	MDVector<T,D> operator-(V val) const;
	template <class T2>
	MDVector<T,D> operator-(const MDVector<T2,D> &p) const;
	template<class V>
	MDVector<T,D> &operator-=(V val);
	template<class T2>
	MDVector<T,D> &operator-=(const MDVector<T2,D> &p);
	
	template<class V>
	MDVector<T,D> operator*(V scale) const;
	template<class V>
	MDVector<T,D> &operator*=(V scale);
	template<class V>
	MDVector<T,D> operator/(V scale) const;
	template<class V>
	MDVector<T,D> &operator/=(V scale);
	
	float length() const;
	T lengthSquared() const;
	//float distance(const MDVector<T,D> &p) const;
	//double distance2(const MDVector<T,D> &p) const;
	
	void normalize();
	MDVector<T,D> normalized() const;
	
	T dotProduct(const MDVector<T,D> &p) const;
	MDVector<T,D> operator*(const MDVector<T,D> &p) const;
	//MDVector<T,D> crossProduct(const MDVector<T,D> &p) const;
	//MDVector<T,D> operator^(const MDVector<T,D> &p) const { return crossProduct(p); };
	
	template<class V>
	void rotate(const MDVector<V,D> &direction);

	template <class T2>
	friend MDVector<T2, 3> crossProduct(const MDVector<T2, 3> &a, const MDVector<T2, 3> &b);
};

template <class T2, unsigned int D2, class V>
MDVector<T2, D2> operator* (V v, const MDVector<T2,D2> &p) { return p * v; }

template <class T, unsigned int D, unsigned int N>
MDVector<T, D> center(const MDVector< MDVector<T, D>, N> &vertices);

template <class T, unsigned int D, unsigned int N>
MDVector<T, D> normal(const MDVector< MDVector<T, D>, N> &vertices);

template <class T, unsigned int D, unsigned int N, class Precision>
MDVector<Precision, D> createUnitNormal(const MDVector< MDVector<T, D>, N> &vertices);

#include "MDVector.hpp"

#endif
