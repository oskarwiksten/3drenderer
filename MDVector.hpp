
#include <cassert>
#include "util.h"
#include <cmath>
#include <limits>

#define JOIN( X, Y ) JOIN2(X,Y)
#define JOIN2( X, Y ) X##Y

namespace custom_static_assert
{
    template <bool> struct STATIC_ASSERT_FAILURE;
    template <> struct STATIC_ASSERT_FAILURE<true> { enum { value = 1 }; };

    template<int x> struct static_assert_test{};
}

#define COMPILE_ASSERT(x) \
    typedef ::custom_static_assert::static_assert_test<\
        sizeof(::custom_static_assert::STATIC_ASSERT_FAILURE< (bool)( x ) >)>\
            JOIN(_static_assert_typedef, __LINE__)


template<class T, unsigned int D>
const MDVector<T,D> MDVector<T,D>::zero;

#define FOREACH(D) for(unsigned int i = 0; i < (D); ++i)

template<class T, unsigned int D>
MDVector<T,D>::MDVector() {
	assert(D > 0);
	/*FOREACH(D) {
		v[i] = 0;
	} */
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D>::MDVector(const T2 v_[D]) {
	assert(D > 0);
	*this = v_;
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D>::MDVector(T2 x) {
	assert(D == 1);
	v[0] = x;
}

template<class T, unsigned int D>
template<class T2, class T3>
MDVector<T,D>::MDVector(T2 x, T3 y) {
	assert(D >= 2);
	v[0] = x;
	v[1] = y;
}

template<class T, unsigned int D>
template<class T2, class T3, class T4>
MDVector<T,D>::MDVector(T2 x, T3 y, T4 z) {
	assert(D >= 3);
	v[0] = x;
	v[1] = y;
	v[2] = z;
}

template<class T, unsigned int D>
template<class T2, class T3, class T4, class T5>
MDVector<T,D>::MDVector(T2 a, T3 b, T4 c, T5 d) {
	assert(D >= 4);
	v[0] = a;
	v[1] = b;
	v[2] = c;
	v[3] = d;
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D>::MDVector(const MDVector<T2,D> &p) {
	*this = p;
}

template<class T, unsigned int D>
std::ostream &operator<<(std::ostream &os, const MDVector<T,D> &p) {
	os << '[' << p.v[0];
	for(unsigned int i = 1; i < (D); i++) {
		os << ", " << p.v[i];
	}
	return os << ']';
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D> &MDVector<T,D>::operator=(const MDVector<T2,D> &p) {
	FOREACH(D) {
		v[i] = p[i];
	}
	return *this;
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D> &MDVector<T,D>::operator=(const T2 v_[D]) {
	FOREACH(D) {
		v[i] = v_[i];
	}
	return *this;
}

template<class T, unsigned int D>
template<class T2>
bool MDVector<T,D>::operator==(const MDVector<T2,D> &p) const {
	FOREACH(D) {
		if (v[i] != p[i]) return false;
	}
	return true;
}

template<class T, unsigned int D>
template<class T2>
bool MDVector<T,D>::operator==(const T2 v_[D]) const {
	FOREACH(D) {
		if (v[i] != v_[i]) return false;
	}
	return true;
}

template<class T, unsigned int D>
template<class T2>
bool MDVector<T,D>::operator!=(const MDVector<T2,D> &p) const {
	return !(operator==(p.v));
}

template<class T, unsigned int D>
template<class T2>
bool MDVector<T,D>::operator!=(const T2 v_[D]) const {
	return !(operator==(v));
}

template<class T, unsigned int D>
inline T &MDVector<T,D>::operator[](unsigned int d) {
	assert(d < D);
	return v[d];
}

template<class T, unsigned int D>
inline const T &MDVector<T,D>::operator[](unsigned int d) const {
	assert(d < D);
	return v[d];
}

template<class T, unsigned int D>
void MDVector<T,D>::opposite() {
	FOREACH(D) {
		v[i] = -v[i];
	}
}

template<class T, unsigned int D>
MDVector<T,D> MDVector<T,D>::getOpposite() const {
	MDVector<T,D> result(*this);
	result.opposite();
	return result;
}

template<class T, unsigned int D>
bool MDVector<T,D>::operator!() const {
	FOREACH(D) {
		if (v[i]) return false;
	}
	return true;
}

template<class T, unsigned int D>
MDVector<T,D> & MDVector<T,D>::operator++() {
	FOREACH(D) {
		v[i]++;
	}
	return *this;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> MDVector<T,D>::operator+(V val) const {
	MDVector<T,D> result(*this);
	result += val;
	return result;
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D> MDVector<T,D>::operator+(const MDVector<T2,D> &d) const {
	MDVector<T,D> result(*this);
	result += d;
	return result;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> & MDVector<T,D>::operator+=(V val) {
	FOREACH(D) {
		v[i] = static_cast<T>(v[i] + val);
	}
	return *this;
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D> & MDVector<T,D>::operator+=(const MDVector<T2,D> &d) {
	FOREACH(D) {
		v[i] = static_cast<T>(v[i] + d[i]);
	}
	return *this;
}

template<class T, unsigned int D>
MDVector<T,D> MDVector<T,D>::operator-() const {
	MDVector<T,D> result(*this);
	FOREACH(D) {
		result[i] = -result[i];
	}
	return result;
}

template<class T, unsigned int D>
MDVector<T,D> & MDVector<T,D>::operator--() {
	FOREACH(D) {
		v[i]--;
	}
	return *this;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> MDVector<T,D>::operator-(V val) const {
	return operator+(-val); 
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D> MDVector<T,D>::operator-(const MDVector<T2,D> &d) const {
	MDVector<T,D> result(*this);
	result -= d;
	return result;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> & MDVector<T,D>::operator-=(V val) {
	return operator+=(-val); 
}

template<class T, unsigned int D>
template<class T2>
MDVector<T,D> & MDVector<T,D>::operator-=(const MDVector<T2,D> &d) {
	FOREACH(D) {
		v[i] = static_cast<T>(v[i] - d.v[i]);
	}
	return *this;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> MDVector<T,D>::operator*(V scale) const {
	MDVector<T,D> result(*this);
	result *= scale;
	return result;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> & MDVector<T,D>::operator*=(V scale) {
	FOREACH(D) {
		v[i] = static_cast<T>(v[i] * scale);
	}
	return *this;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> MDVector<T,D>::operator/(V scale) const {
	MDVector<T,D> result(*this);
	result /= scale;
	return result;
}

template<class T, unsigned int D>
template<class V>
MDVector<T,D> & MDVector<T,D>::operator/=(V scale) {
	if (!scale) return *this;
	FOREACH(D) {
		v[i] = static_cast<T>(v[i] / scale);
	}
	return *this;
}

template<class T, unsigned int D>
float MDVector<T,D>::length() const {
	return sqrtf(lengthSquared());
}

template<class T, unsigned int D>
T MDVector<T,D>::lengthSquared() const {
	return this->dotProduct(*this);
}

/*
template<class T, unsigned int D>
float MDVector<T,D>::distance(const MDVector<T,D> &p) const {
	return sqrtf(distance2(p));
}

template<class T, unsigned int D>
double MDVector<T,D>::distance2(const MDVector<T,D> &p) const {
	double result = 0, t;
	FOREACH(D) {
		t = v[i] - p.v[i];
		result += (t * t);
	}
	return result;
}
*/

template<class T, unsigned int D>
void MDVector<T,D>::normalize() {
	COMPILE_ASSERT(!std::numeric_limits<T>::is_integer);
	*this /= sqrt(lengthSquared());
}

template<class T, unsigned int D>
MDVector<T,D> MDVector<T,D>::normalized() const {
	MDVector<T,D> result(*this);
	result.normalize();
	return result;
}

template<class T, unsigned int D>
T MDVector<T,D>::dotProduct(const MDVector<T,D> &p) const {
	T result = 0;
	FOREACH(D) {
		result += v[i] * p.v[i];
	}
	return result;
}

template<class T, unsigned int D>
MDVector<T,D> MDVector<T,D>::operator*(const MDVector<T,D> &p) const {
	MDVector<T,D> result;
	FOREACH(D) {
		result[i] = v[i] * p.v[i];
	}
	return result;
}

template <class T>
MDVector<T, 3> crossProduct(const MDVector<T, 3> &a, const MDVector<T, 3> &b) {
	// (a2b3 - a3b2, a3b1 - a1b3, a1b2 - a2b1).
	// (a1b2 - a2b1, a2b0 - a0b2, a0b1 - a1b0).
	return MDVector<T, 3>(
			a[1] * b[2] - a[2] * b[1]
			,a[2] * b[0] - a[0] * b[2]
			,a[0] * b[1] - a[1] * b[0]
		);
}

template <class T, unsigned int D, unsigned int N>
MDVector<T, D> center(const MDVector< MDVector<T, D>, N> &vertices) {
	MDVector<T, D> c(vertices[0]);
	for(unsigned int i = 1; i < N; ++i) {
		c += vertices[i];
	}
	c /= N;
	return c;
}

template <class T, unsigned int D, unsigned int N>
MDVector<T, D> createUnitNormal(const MDVector< MDVector<T, D>, N> &vertices) {
	/* 
	                 p0
	                 .
	         __..--'' '-.
	  p2  ----------------  p1
	
	*/
	assert(N >= 3);
	return crossProduct((vertices[0] - vertices[1]), (vertices[2] - vertices[1]));
}

template <class T, unsigned int D, unsigned int N, class Precision>
MDVector<Precision, D> createUnitNormal(const MDVector< MDVector<T, D>, N> &vertices) {
	assert(N >= 3);
	const MDVector<Precision, D> a(vertices[0] - vertices[1]);
	const MDVector<Precision, D> b(vertices[2] - vertices[1]);
	return crossProduct(a, b).normalized();
}

