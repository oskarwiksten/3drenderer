CC = g++
LD = g++
CPPFLAGS = `sdl-config --cflags` -Wall -O2 -fopenmp
#-ggdb
LDFLAGS = `sdl-config --libs` -lSDL_image -lgomp
# -pg
RM   = /bin/rm -f
OBJS = main.o DrawingArea.o DObject.o Renderer_sw.o util.o DSolids.o Lighting.o DObjectCollection.o DSurface.o SceneAnimator.o Material.o
PROG = sdlrender
VERS = 0.1

.PHONY: clean
all: $(PROG)
$(PROG): $(OBJS)
	$(LD) $(LDFLAGS) -o $(PROG) $(OBJS) 
gfxtest: gfxtest.o DrawingArea.o Material.o
	$(LD) $(LDFLAGS) -o gfxtest gfxtest.o DrawingArea.o Material.o
clean:
	$(RM) *~ $(OBJS) $(PROG) mathtest mathtest.o gfxtest gfxtest.o
.PHONY: clean
