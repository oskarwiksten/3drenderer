#include "Material.h"
#include <SDL/SDL_image.h>

Texture::Texture(const std::string &filename) {
	image = IMG_Load(filename.c_str());
	if (image == NULL) std::cerr << "Unable to load image!" << std::endl;
	
	corners[0] = TexelPosition(0, 0);
	Uint16 w = 0, h = 0;
	if (image != NULL) {
		w = image->w - 1;
		h = image->h - 1;
	}
	corners[1] = TexelPosition(w, 0);
	corners[2] = TexelPosition(w, h);
	corners[3] = TexelPosition(0, h);
}

Texture::~Texture() {
	if (image != NULL) {
		SDL_FreeSurface(image);
	}
}

static const float D_c_diffuse = 0.5f;
static const float D_c_specular = 0.6f;
static const Uint8 D_c_specular_shininess = 10;
static const float D_c_ambient = 0.2f;


Material::Material(const PixelColor baseColor_) : texture(NULL), bumpmapTexture(NULL), baseColor(baseColor_) {
	c_diffuse = D_c_diffuse;
	c_specular = D_c_specular;
	c_specular_shininess = D_c_specular_shininess;
	c_ambient = D_c_ambient;
}

Material::Material(const PixelColor baseColor_, const Texture *texture_, const TexturePositionSet &texturePositions_
	, const Texture *bumpmapTexture_, const TexturePositionSet &bumpmapTexturePositions_) 
	: texture(texture_)
	, texturePositions(texturePositions_) 
	, bumpmapTexture(bumpmapTexture_)
	, bumpmapTexturePositions(bumpmapTexturePositions_) 
	, baseColor(baseColor_) {
	c_diffuse = D_c_diffuse;
	c_specular = D_c_specular;
	c_specular_shininess = D_c_specular_shininess;
	c_ambient = D_c_ambient;
}


void Texture::processBumpmap() {
	Uint8 **heights = new Uint8*[image->h];
	Uint8 **dx = new Uint8*[image->h];
	Uint8 **dy = new Uint8*[image->h];
	for(Uint16 y = 0; y < image->h; ++y) {
		heights[y] = new Uint8[image->w];
		dx[y] = new Uint8[image->w];
		dy[y] = new Uint8[image->w];
	}
	Uint8 r, g, b;
	SDL_PixelFormat *pixelFormat = image->format;
	
	for(Uint16 y = 0; y < image->h; ++y) {
		for(Uint16 x = 0; x < image->w; ++x) {
			SDL_GetRGB(getpixel(image, x, y), pixelFormat, &r, &g, &b);
			heights[y][x] = static_cast<Uint8>((static_cast<Uint16>(r) + static_cast<Uint16>(g) + static_cast<Uint16>(b)) / 3);
			dx[y][x] = 127;
			dy[y][x] = 127;
		}
	}
	
	for(Uint16 y = 1; y < image->h-1; ++y) {
		for(Uint16 x = 1; x < image->w-1; ++x) {
			const Sint16 ddx = (static_cast<Sint16>(heights[y][x-1]) - static_cast<Sint16>(heights[y][x+1]));
			dx[y][x] = 127 + (ddx / 2);
			const Sint16 ddy = (static_cast<Sint16>(heights[y-1][x]) - static_cast<Sint16>(heights[y+1][x]));
			dy[y][x] = 127 + (ddy / 2);
		}
	}
	
	for(Uint16 y = 0; y < image->h; ++y) {
		for(Uint16 x = 0; x < image->w; ++x) {
			putpixel(image, x, y, SDL_MapRGB(pixelFormat, dx[y][x], dy[y][x], 0));
		}
	}
	
	for(Uint16 y = 0; y < image->h; ++y) {
		delete[] heights[y];
		delete[] dy[y];
		delete[] dx[y];
	}
	delete[] heights;
	delete[] dy;
	delete[] dx;
}

void Texture::empower() {
	Uint8 **heights = new Uint8*[image->h];
	for(Uint16 y = 0; y < image->h; ++y) {
		heights[y] = new Uint8[image->w];
	}
	Uint8 r, g, b;
	SDL_PixelFormat *pixelFormat = image->format;
	
	Uint8 maxheight = 0, minheight = 255;
	for(Uint16 y = 0; y < image->h; ++y) {
		for(Uint16 x = 0; x < image->w; ++x) {
			SDL_GetRGB(getpixel(image, x, y), pixelFormat, &r, &g, &b);
			heights[y][x] = static_cast<Uint8>((static_cast<Uint16>(r) + static_cast<Uint16>(g) + static_cast<Uint16>(b)) / 3);
			if (heights[y][x] > maxheight) maxheight = heights[y][x];
			if (heights[y][x] < minheight) minheight = heights[y][x];
		}
	}
	
	for(Uint16 y = 0; y < image->h; ++y) {
		for(Uint16 x = 0; x < image->w; ++x) {
			const Uint8 v = static_cast<float>(heights[y][x] - minheight) * 255.0f / (maxheight - minheight);
			putpixel(image, x, y, SDL_MapRGB(pixelFormat, v, v, v));
		}
	}
	
	for(Uint16 y = 0; y < image->h; ++y) {
		delete[] heights[y];
	}
	delete[] heights;
}