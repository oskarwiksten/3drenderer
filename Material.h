#ifndef __Material_h__
#define __Material_h__

#include <SDL/SDL.h>
#include "DrawingArea.h"
#include "MDVector.h"

typedef PixelCoordinate TexelCoordinate;
typedef Uint32 PixelColor;

class Texture {
public:
	typedef MDVector<TexelCoordinate, 2> TexelPosition;
	
	SDL_Surface *image;
	TexelPosition corners[4];

public:
	Texture(const std::string &filename);
	~Texture();
	
	const TexelPosition &topLeft() 		const { return corners[0]; }
	const TexelPosition &topRight() 	const { return corners[1]; }
	const TexelPosition &bottomRight() 	const { return corners[2]; }
	const TexelPosition &bottomLeft() 	const { return corners[3]; }
	
	void empower();
	void processBumpmap();
};

Texture loadBumpmap(const std::string &filename);

class Material {
public:
	typedef MDVector<TexelCoordinate, 2> TexturePosition;
	typedef MDVector<TexturePosition, 3> TexturePositionSet;
	
	const Texture *texture;
	TexturePositionSet texturePositions;
	const Texture *bumpmapTexture;
	TexturePositionSet bumpmapTexturePositions;

	PixelColor baseColor;
	float c_diffuse;
	float c_specular;
	float c_specular_shininess;
	float c_ambient;

public:
	Material(const PixelColor baseColor_);
	Material(const PixelColor baseColor_, const Texture *texture_, const TexturePositionSet &texturePositions_
			, const Texture *bumpmapTexture_, const TexturePositionSet &bumpmapTexturePositions_);
};


#endif
