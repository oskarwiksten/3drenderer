# Software 3d renderer

An 3d renderer and animator witten in C++.
This was written as an experiment back in 2009 to see if a pure-software renderer could perform well, and to learn about shading models. The renderer only uses putpixel (see `DrawingArea::putpixel`) from sdl to do its drawing.

![teapot](teapot.png)

Features:
* Uses SDL to plot single pixels
* Does not use any built in functions other than placing single pixels
* Program determines all colors by interpolation and its own lightning models

3d features:
* Simple triangles make up the objects
* Z-buffering for smooth triangle edges
* Allows user to select different lightning models

## To build
	apt-get install libsdl1.2-dev libsdl-image1.2-dev
	make

## To run
	./sdlrender

## Keys
	q, ESQ - quit

	right mouse btn - select object
	s - select next object
	a - extend selection to parent object
	arrow keys - rotate selected object
	left mouse btn - rotate selected object
	ctrl + left mouse btn - resize selected object

	space - pause animator
	. - tick animator one step
	, - tick animator one step backwards

	w - render as wireframes
	e - render as surfaces
	n - draw normals from each surface
	l - switch lightning modes

	p - print scene to stdout
	d - display debug output

## License
GPLv3

