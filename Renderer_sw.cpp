#include "Renderer_sw.h"
#include "DrawingArea.h"
#include <algorithm>

static const DSurface::Vertex eye(0, 0, Renderer_sw::constant3D);

Renderer_sw::Renderer_sw(const PixelCoordinate width_, const PixelCoordinate height_) :
		surface(NULL)
		,lighting(NULL)
		,drawNormals(false)
		,lastRenderingTicks(0) {
	resize(width_, height_);
}

static SolidColorLighting vertexNormalLighting;
static SolidColorLighting surfaceNormalLighting;

void Renderer_sw::resize(const PixelCoordinate width_, const PixelCoordinate height_) {
	if (width == width_ || height == height_) return;
	width = width_;
	height = height_;
	if ((surface = SDL_SetVideoMode(width, height, 0, SDL_RESIZABLE)) == NULL) {
		printf("SDL_SetVideoMode() error: %s\n", SDL_GetError());
		exit (-1);
	}
	
	drawingArea.setSurface(surface);
	
	vertexNormalLighting.setColor(SDL_MapRGB(surface->format, 0x77, 0xff, 0x77));
	surfaceNormalLighting.setColor(SDL_MapRGB(surface->format, 0x77, 0x77, 0xff));

	center = PixelPosition(width_ / 2, height_ / 2);
	if (lighting != NULL) {
		lighting->initialize(surface->format, eye, center);
	}
	zbuffer.setBufferSize(width_, height_);
}

void Renderer_sw::setLighting(Lighting *lighting_) {
	lighting = lighting_;
	drawingArea.clearScanlineListeners();
	if (lighting == NULL) return;
	
	lighting->initialize(surface->format, eye, center);
	lighting->addScanLineListeners(drawingArea);
	zbuffer.setLighting(lighting_);
	zbuffer.addScanLineListeners(drawingArea);
}

/*
void setVisibilityEvaluator(VisibilityEvaluator *visibilityEvaluator_) {
	visibilityEvaluator = visibilityEvaluator_;
}
*/


bool Renderer_sw::translate(const DObject::Vertex &v, PixelPosition &p) const {
	RenderingAccuracy d = constant3D - static_cast<RenderingAccuracy>(v[2]);
	
	if (d <= 0) {
		// Point is behind "camera"
		return false;
	} else {
		/*
		         px
		      +-----+    <-- screen
		     z|    /
		      |   /
		      +--+ x     <-- vertex
		(C-z) | /
		      |/
		    C +          <-- eye
		
		(C-z) / x = C / px 
		=>
		px = (C * x) / (C - z)
		
		*/
		
		// pixel x = (x * C) / (C - z)
		p[0] = static_cast<PixelCoordinate>(static_cast<RenderingAccuracy>(v[0]) * constant3D / d);
		// pixel y = (-y * C) / (C - z)
		p[1] = static_cast<PixelCoordinate>(static_cast<RenderingAccuracy>(-v[1]) * constant3D / d);
	}
	p += center;
	return (p[0] < width) && (p[1] < height);
}

bool Renderer_sw::translate(const DSurface &surface, PixelPositionTriangle &t) const {
	for(Uint8 i = 0; i < surface.N; ++i) {
		if (!translate(surface.vertices[i], t[i])) return false;
	}
	return true;
	/*
	return 
		   translate(surface.vertices[0], t[0])
		&& translate(surface.vertices[1], t[1])
		&& translate(surface.vertices[2], t[2]);
	*/
}

struct AscendingCoordinateSorting {
     bool operator()(const DObject::Surface& a, const DObject::Surface& b) {
          //return b.center()[2] < a.center()[2];
          return 
				(b.vertices[0][2] + b.vertices[1][2] + b.vertices[2][2]) 
				>
				(a.vertices[0][2] + a.vertices[1][2] + a.vertices[2][2]);
     }
};

static const float normalLength = 20.0f;

void Renderer_sw::drawEdgeNormals(const DSurface &s, const PixelPositionTriangle &pixelPositions) {
	DSurface::NormalSet n = s.normals;
	for(Uint8 i = 0; i < s.N; ++i) {
		n[i].normalize();
	}
	DSurface::VertexSet p = s.vertices + n * normalLength;

	drawingArea.setPixelColorEvaluator(vertexNormalLighting);
	for(Uint8 i = 0; i < s.N; ++i) {
		PixelPosition pos;
		if (translate(p[i], pos)) {
			drawingArea.drawLine(pos, pixelPositions[i]);
		}
	}
}

void Renderer_sw::drawSurfaceNormals(const DSurface &s) {
	const DObject::NormalVector n = s.getCenterUnitNormal() * normalLength;
	const DObject::Vertex center = s.getCenter();
	const DObject::NormalVector p = n + center;
	
	drawingArea.setPixelColorEvaluator(surfaceNormalLighting);
	
	PixelPosition centerpos, pos;
	if (translate(p, pos) && translate(center, centerpos)) {
		drawingArea.drawLine(pos, centerpos);
	}
}

void Renderer_sw::renderSurface(const DSurface &s) {
	PixelPositionTriangle pixelPositions;
	if (!translate(s, pixelPositions)) return;
	
	zbuffer.setCurrentSurface(s);
	drawingArea.setPixelColorEvaluator(zbuffer);
	
	switch (s.getRenderMode()) {
		case DSurface::RenderMode::SURFACE:
			drawingArea.fillTriangle(pixelPositions); break;
		case DSurface::RenderMode::WIREFRAME:
			drawingArea.drawTriangle(pixelPositions); break;
	}
	if (drawNormals) {
		drawEdgeNormals(s, pixelPositions);
		// TODO: surface normals are drawn twice for visible surfaces.
		drawSurfaceNormals(s);
	}
}



static bool isFacingAway(const DObject::Surface& s) {
	if (s.getRenderMode() == DSurface::RenderMode::WIREFRAME) return false;
	
	float theta = 
		s.getCenterUnitNormal()
		.dotProduct(
			eye - s.getCenter()
		);
	
	// if positive then normal is towards eye, acos(theta) would be < M_PI
	return (theta <= 0);
}


static void backfaceCulling(DObject::SurfaceCollection &objects) {
	objects.erase(remove_if(objects.begin(), objects.end(), isFacingAway), objects.end());
}

void Renderer_sw::render(const DObjectCollection &objectCollection) {
	static Uint8 renderedFrame = 0;
	const bool debugOutput = (renderedFrame == 0);
	++renderedFrame;
	if (renderedFrame == 50) renderedFrame = 0;
	
	const Uint32 startTicks = SDL_GetTicks();
	if (surface != NULL) SDL_FillRect(surface, NULL, 0);
	
	zbuffer.clearBuffer();
	
	DObject::SurfaceCollection objects;
	objectCollection.getSurfaces(objects);
	const size_t totalSurfaces = objects.size();
	
	if (drawNormals) {
		#pragma omp parallel for 
		for(unsigned int j = 0; j < totalSurfaces; ++j) {
			drawSurfaceNormals(objects[j]);
		}
	}

	backfaceCulling(objects);
	const size_t visibleSurfaces = objects.size();
	
	if (drawNormals) {
		// Sort all surfaces on z so that surfaces farthest away will be drawn first
		std::sort(objects.begin(), objects.end(), AscendingCoordinateSorting());
	}

	//const unsigned int N = objects.size();
	//for(int i = 0; i < N; ++i) {
	//	DSurface &s = objects[i];
	for(DObject::SurfaceCollection::iterator i = objects.begin(); i != objects.end(); ++i) {
		renderSurface(*i);
	}
	
	if (debugOutput) {
		lastRenderingTicks = SDL_GetTicks() - startTicks;
		std::cout 	<< totalSurfaces << " surfaces, " 
					<< visibleSurfaces << " visible. " 
					<< objectCollection.getNumberOfVertices() << " vertices. " 
					<< lastRenderingTicks << "ms."
					<< std::endl;
	}
	
	drawingArea.update();
}


class PixelQueryLighting : public Lighting {
public:
	DSurfaceObject *pixelQueryResult;
	const PixelPosition pixelQueryPosition;
	const DSurface *currentSurface;

	PixelQueryLighting(const PixelPosition &pixelQueryPosition_) 
		:pixelQueryResult(NULL)
		,pixelQueryPosition(pixelQueryPosition_) 
		,currentSurface(NULL) {
	}
	virtual ~PixelQueryLighting() {}
	virtual void setCurrentSurface(const DSurface &surface) {
		currentSurface = &surface;
	}
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_) {
		if (p == pixelQueryPosition) {
			pixelQueryResult = currentSurface->owner;
		}
		return false;
	}
};

DSurfaceObject *Renderer_sw::pixelQuery(const PixelPosition &p, const DObjectCollection &scene) {
	Lighting *currentLighting = lighting;
	SDL_Surface *currentSurface = surface;
	PixelQueryLighting query(p);
	setLighting(&query);
	drawingArea.setSurface(NULL);
	//setSurface(NULL);
	render(scene);
	//setSurface(currentSurface);
	drawingArea.setSurface(currentSurface);

	setLighting(currentLighting);
	return query.pixelQueryResult;
}

