#ifndef __Renderer_sw_h__
#define __Renderer_sw_h__

#include <SDL/SDL.h>

#include "DObject.h"
#include "DObjectCollection.h"
#include "DrawingArea.h"
#include "Lighting.h"

class Renderer_sw {
public:
	typedef ObjectCoordinate RenderingAccuracy;
	static const RenderingAccuracy constant3D = 500;

private:
	SDL_Surface *surface;
	Lighting *lighting;
	PixelCoordinate width;
	PixelCoordinate height;
	PixelPosition center;
	ZBufferEvaluator zbuffer;
	DrawingArea drawingArea;

	bool translate(const DObject::Vertex &v, PixelPosition &p) const;
	bool translate(const DSurface &surface, PixelPositionTriangle &t) const;
	void drawEdgeNormals(const DSurface &s, const PixelPositionTriangle &pixelPositions);
	void drawSurfaceNormals(const DSurface &s);

	void renderSurface(const DSurface &s);

public:
	bool drawNormals;
	Uint32 lastRenderingTicks;

	Renderer_sw(const PixelCoordinate width_, const PixelCoordinate height_);
	void resize(const PixelCoordinate width_, const PixelCoordinate height_);
	void setLighting(Lighting *lighting_);
	SDL_Surface *getSurface() const { return surface; }
	Lighting *getLighting() const { return lighting; }

	void render(const DObjectCollection &objectCollection);
	DSurfaceObject *pixelQuery(const PixelPosition &p, const DObjectCollection &scene);
	void prepareTexture(const Texture &t);
};

#endif
