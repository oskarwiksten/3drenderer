#ifndef __Rotation_h__
#define __Rotation_h__

#include "MDVector.h"
#include "util.h"

template <class T>
class RotationTranslation {
public:
	typedef MathHelper::Precision R;
	typedef MDVector<R, 3> Rotation;
	typedef MDVector<T, 3> Direction;
	
	static const RotationTranslation<T> zero;
	
	Rotation rotation;
	Direction translation;
	RotationTranslation(const Direction &translation_ = Direction::zero, const Rotation &rotation_ = Rotation::zero);
	
	template <class T2>
	void applyRotationTo(MDVector<T2, 3> &point) const;
	template <class T2>
	void applyTranslationTo(MDVector<T2, 3> &point) const;
	template <class T2>
	void applyTo(MDVector<T2, 3> &point) const;
	/*template <class T2, unsigned int N>
	void applyToAll(MDVector< MDVector<T2>, N> &vertices) const;*/

	void operator+=(const RotationTranslation<T> &r);
	void operator-=(const RotationTranslation<T> &r);
	RotationTranslation<T> operator-() const;
	
};

#include "Rotation.hpp"

#endif
