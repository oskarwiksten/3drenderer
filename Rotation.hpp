
#include "util.h"

template<class T>  
const RotationTranslation<T> RotationTranslation<T>::zero;

template<class T>
RotationTranslation<T>::RotationTranslation(const Direction &translation_, const Rotation &rotation_) :
		rotation(rotation_)
		,translation(translation_) {
}

template<class T>
template<class T2>
void RotationTranslation<T>::applyRotationTo(MDVector<T2, 3> &point) const {
	if (!rotation) return;
	MDVector<T, 3> p(point);
	
	R c = MathHelper::cos(rotation[0]);
	R s = MathHelper::sin(rotation[0]);
	//point[0] = p[0];
	point[1] = static_cast<T>(p[1] *  c + p[2] *  s);
	point[2] = static_cast<T>(p[1] * -s + p[2] *  c);
	
	c = MathHelper::cos(rotation[1]);
	s = MathHelper::sin(rotation[1]);
	p[0] = static_cast<T>(point[0] *  c + point[2] * -s);
	p[1] = point[1];
	p[2] = static_cast<T>(point[0] *  s + point[2] *  c);
	
	c = MathHelper::cos(rotation[2]);
	s = MathHelper::sin(rotation[2]);
	point[0] = static_cast<T>(p[0] *  c + p[1] *  s);
	point[1] = static_cast<T>(p[0] * -s + p[1] *  c);
	point[2] = p[2];
}

template<class T>
template<class T2>
void RotationTranslation<T>::applyTranslationTo(MDVector<T2, 3> &point) const {
	point += translation;
}

template<class T>
template<class T2>
void RotationTranslation<T>::applyTo(MDVector<T2, 3> &point) const {
	applyRotationTo(point);
	applyTranslationTo(point);
}

template<class T>
void RotationTranslation<T>::operator+=(const RotationTranslation<T> &r) {
	rotation += r.rotation;
	translation += r.translation;
}

template<class T>
void RotationTranslation<T>::operator-=(const RotationTranslation &r) {
	rotation -= r.rotation;
	translation -= r.translation;
}

template<class T>
RotationTranslation<T> RotationTranslation<T>::operator-() const {
	return RotationTranslation<T>(-translation, -rotation);
}
