#include "SceneAnimator.h"

SceneAnimator::SceneAnimator() {}

SceneAnimator::~SceneAnimator() {
	for (AnimationList::const_iterator i = animations.begin(); i != animations.end(); ++i) {
		Animation *a = *i;
		delete a;
	}
}

void SceneAnimator::addConstantAnimation(DObjectCollection &scene, const std::string &objectName, const Displacement &displacement) {
	DObjectCollection::DisplacedObject* obj = scene.findObject(objectName);
	if (obj == NULL) {
		std::cout << "Cannot find object " << objectName << " to animate!" << std::endl;
		return;
	}
	
	addAnimation(new ConstantAnimation(obj, displacement));
}

void SceneAnimator::addConstantAnimation(DObjectCollection &scene, const std::string &objectName, const SceneAnimator::Rotation &rotation) {
	addConstantAnimation(scene, objectName, Displacement(Displacement::Direction::zero, rotation));
}

void SceneAnimator::addAnimation(Animation *animation) {
	animations.push_back(animation);
}

void SceneAnimator::tick(const bool forward) const {
	for (AnimationList::const_iterator i = animations.begin(); i != animations.end(); ++i) {
		(*i)->tick(forward);
	}
}

ConstantAnimation::ConstantAnimation(DObjectCollection::DisplacedObject *obj_, const Displacement &displacement_) 
	: obj(obj_)
	, displacement(displacement_) {
}

void ConstantAnimation::tick(const bool forward) {
	obj->displacement += forward ? displacement : -displacement;
}

BouncingAnimation::BouncingAnimation(DObjectCollection::DisplacedObject *obj_, const Displacement::Direction &boundingBox_, const Displacement::Direction &speed) 
	: obj(obj_)
	, boundingBox(boundingBox_)
	, direction(speed) {
}

void BouncingAnimation::tick(const bool forward) {
	Displacement::Direction &position = obj->displacement.translation;
	
	if (forward) {
		direction[1]--;
		position += direction;
	} else {
		direction[1]++;
		position -= direction;
	}
	
	static const ObjectCoordinate min_x = -boundingBox[0];
	static const ObjectCoordinate max_x = boundingBox[0];
	static const ObjectCoordinate min_z = -boundingBox[2];
	static const ObjectCoordinate max_z = boundingBox[2];
	static const ObjectCoordinate min_y = -boundingBox[1];
	
	if (position[0] < min_x) {
		direction[0] = -direction[0];
		position[0] = min_x - position[0] + min_x;
	} else if (position[0] > max_x) {
		direction[0] = -direction[0];
		position[0] = max_x -position[0] + max_x;
	}
	
	if (position[2] < min_z) {
		direction[2] = -direction[2];
		position[2] = min_z - position[2] + min_z;
	} else if (position[2] > max_z) {
		direction[2] = -direction[2];
		position[2] = max_z - position[2] + max_z;
	}
	
	if (position[1] < min_y) {
		direction[1] = -direction[1];
		position[1] = min_y - position[1] + min_y;
	}
}

