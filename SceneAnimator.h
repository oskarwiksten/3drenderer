#ifndef __SceneAnimator_h__
#define __SceneAnimator_h__

#include "DSurface.h"
#include "DObjectCollection.h"
#include <string>
#include <vector>

class Animation {
public:
	~Animation() {}
	virtual void tick(const bool forward) = 0;
};

class SceneAnimator {
public:
	typedef Displacement::Rotation Rotation;
	typedef Displacement::Direction Translation;

private:
	typedef std::vector<Animation*> AnimationList;

	AnimationList animations;

public:
	SceneAnimator();
	~SceneAnimator();

	void addConstantAnimation(DObjectCollection &scene, const std::string &objectName, const Displacement &displacement); 
	void addConstantAnimation(DObjectCollection &scene, const std::string &objectName, const Rotation &rotation); 
	void addAnimation(Animation *animation); 

	void tick(const bool forward) const;
};

class ConstantAnimation : public Animation {
private:
	DObjectCollection::DisplacedObject *obj;
	Displacement displacement;
public:
	ConstantAnimation(DObjectCollection::DisplacedObject *obj_, const Displacement &displacement_);
	~ConstantAnimation() {}
	virtual void tick(const bool forward);
};


class BouncingAnimation : public Animation {
private:
	DObjectCollection::DisplacedObject *obj;
	Displacement::Direction boundingBox;
	Displacement::Direction direction;
public:
	BouncingAnimation(DObjectCollection::DisplacedObject *obj_, const Displacement::Direction &boundingBox_, const Displacement::Direction &speed);
	~BouncingAnimation() {}
	virtual void tick(const bool forward);
};

#endif
