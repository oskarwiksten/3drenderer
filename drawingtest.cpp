#include <SDL/SDL.h>
#include "drawing.h"

int main(int argc, char **argv) {
	MathHelper::init();

	/* Audio and video init */
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_init() error: %s\n", SDL_GetError());
		exit (-1);
	}
	
	/* At program exit SDL_Quit will make a clean up */
    atexit(SDL_Quit);

	if ((surface = SDL_SetVideoMode(800, 600, 0, SDL_RESIZABLE)) == NULL) {
		printf("SDL_SetVideoMode() error: %s\n", SDL_GetError());
		exit (-1);
	}
		
	SDL_WM_SetCaption("SDL_test", NULL);
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

	initializeScene();
}
