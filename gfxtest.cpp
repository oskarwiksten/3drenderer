
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "DrawingArea.h"
#include "DSurface.h"

#include <cassert>
#include <string>
#include <iostream>

static const unsigned int SCREEN_WIDTH = 640;
static const unsigned int SCREEN_HEIGHT = 480;

static SDL_Surface *surface = NULL;
static DrawingArea *drawingArea = NULL;

void draw();

class SolidColor : public PixelColorEvaluator {
public:
	const PixelColor color;
	SolidColor(PixelColor c) : color(c) {}
	virtual ~SolidColor() {}
	virtual bool getPixelColor(const PixelPosition &p, PixelColor &color_) { color_ = color; return true; }
};


int main(int argc, char **argv) {

	/* Audio and video init */
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_init() error: %s\n", SDL_GetError());
		exit (-1);
	}
	
	/* At program exit SDL_Quit will make a clean up */
    atexit(SDL_Quit);

	if ((surface = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_RESIZABLE)) == NULL) {
		printf("SDL_SetVideoMode() error: %s\n", SDL_GetError());
		exit (-1);
	}
	
	SDL_WM_SetCaption("SDL_test", NULL);
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	
	drawingArea = new DrawingArea();
	drawingArea->setSurface(surface);
	const PixelColor white = SDL_MapRGB(surface->format, 0xff, 0xff, 0xff);
	SolidColor s(white);
	drawingArea->setPixelColorEvaluator(s);

	int bpp = surface->format->BytesPerPixel;
	std::cout << "surface bpp=" << bpp << std::endl;
    	
	draw();
	
	bool alive = true;
	while (alive) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				alive = 0;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
					alive = 0; break;

				default:
					break;
				}
				break;
			}
		}
	
		//renderer->render(*scene);
		
		SDL_Delay(20);
	}
	
	delete drawingArea;

	SDL_Quit();
	
	return 0;	
}

void showimage(const PixelPosition &offset, const std::string &filename, const bool bumpmap) {
	Texture t(filename);
	std::cout << "image bpp=" << (int)t.image->format->BytesPerPixel << std::endl;
	if (bumpmap) {
		t.processBumpmap();
	}	
	
	for(int y = 0; y < t.image->h; ++y) {
		for(int x = 0; x < t.image->w; ++x) {
			const PixelPosition p(offset[0] + x, offset[1] + y);
			PixelColor color = getpixel(t.image, x, y);
			drawingArea->putpixel(p, color);
		}
	}
	
	//SDL_FreeSurface(image);
}

void draw() {
	PixelPosition p0(100, 100);
	PixelPosition p1(300, 100);
	PixelPosition p2(100, 200);
	PixelPosition p3(300, 200);
	PixelPositionTriangle t1(p0, p1, p3);
	PixelPositionTriangle t2(p0, p3, p2);
	drawingArea->drawLine(PixelPosition(400, 100), PixelPosition(450, 180));
	drawingArea->fillTriangle(t1);
	drawingArea->fillTriangle(t2);
	
	showimage(PixelPosition(100, 210), "textures/chess.jpg", true);
	showimage(PixelPosition(400, 210), "textures/chess.jpg", false);
	
	drawingArea->update();
}





