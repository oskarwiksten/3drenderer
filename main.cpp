
#include <SDL/SDL.h>
#include "DSolids.h"
#include "Lighting.h"
#include "SceneAnimator.h"
#include "util.h"
#include <cassert>
#include <iostream>

//#define RENDERER_OPENGL
#define RENDERER_SW

#ifdef RENDERER_OPENGL
	#include "Renderer_opengl.h"
	typedef Renderer_opengl Renderer;
#else
	#include "Renderer_sw.h"
	typedef Renderer_sw Renderer;
#endif


void initializeScene();
void deleteScene();

static const unsigned int SCREEN_WIDTH = 800;
static const unsigned int SCREEN_HEIGHT = 600;
static const Uint32 ticksPerFrame = 20;

static Renderer *renderer = NULL;

static PixelColor black, white, selected;
static DObjectCollection *scene;
static SceneAnimator *animator;
static const DObject::Rotation right(0, M_2PI / 50, 0);
static const DObject::Rotation up(M_2PI / 50, 0, 0);
static bool autoRotate = true;
static RotateSelection<DObjectCollection::DisplacedObject *> objectSelection;
static DObjectCollection::DisplacedObject *selectedObject = NULL;

void rotateSelectedObject(const DObject::Rotation &rotation, bool positive) {
	if (selectedObject == NULL) return;
	selectedObject->displacement.rotation += (positive ? rotation: -rotation);
}

void setSelectedObject(DObjectCollection::DisplacedObject *o) {
	if (selectedObject != NULL) selectedObject->object->setColor(white);
	selectedObject = o;
	if (selectedObject != NULL) selectedObject->object->setColor(selected);
}

static int mousedrag_active = 0;
static bool mousedrag_autoRotate = false;

void mousedrag_begin(const SDL_Event &event) {
	if (event.button.button == SDL_BUTTON_RIGHT) {
		DSurfaceObject *obj = renderer->pixelQuery(PixelPosition(event.button.x, event.button.y), *scene);
		if (obj != NULL) obj->setColor(selected);
		setSelectedObject(getDisplacedObject(obj));
	} else if (event.button.button == SDL_BUTTON_LEFT) {
		if (mousedrag_active != 0) return;
		mousedrag_autoRotate = autoRotate;
		autoRotate = false;
		
		SDLMod mod = SDL_GetModState();
		if (mod & KMOD_CTRL) {
			mousedrag_active = 2;
		} else {
			mousedrag_active = 1;
		}
	}
}

void mousedrag_end(const SDL_Event &event) {
	if (mousedrag_active == 0) return;
	autoRotate = mousedrag_autoRotate;
	mousedrag_active = 0;
}

void mousedrag_tick(const SDL_Event &event) {
	if (mousedrag_active == 0) return;
	if (event.motion.xrel == 0 && event.motion.yrel == 0) return;
	if (selectedObject == NULL) return;
	
	if (mousedrag_active == 1) {
		DObject::Rotation rotation(event.motion.yrel * M_2PI / SCREEN_HEIGHT, event.motion.xrel * M_2PI / SCREEN_WIDTH, 0);
		rotateSelectedObject(rotation, true);
	} else if (mousedrag_active == 2) {
		//yrel = -2  -> 0.7f
		//yrel = -1  -> 0.9f
		//yrel = 1   -> 1.1f
		//yrel = 2   -> 1.3f
		//yrel = n   -> 1 + n / 10
		selectedObject->object->scale(1 + static_cast<float>(event.motion.yrel) / 100.0f);
	}
}

void setWireframeMode(const DObject::RenderMode &mode) {
	if (selectedObject == NULL) return;
	selectedObject->object->setRenderMode(mode);
}


typedef RotateSelection< std::pair<Lighting *, std::string> > LightingModelList;
static LightingModelList lightingModels;

void setNextLightingModel() {
	const std::pair<Lighting *, std::string> &m = lightingModels.next();
	
	std::cout << "Current lighting model is now " << m.second << std::endl;
	
	if (renderer == NULL) return;
	#ifdef RENDERER_SW
	renderer->setLighting(m.first);
	#endif
}

void setLightingModel(const std::string &name) {
	while(lightingModels.current().second != name) {
		setNextLightingModel();
	}
}

void extendSelectedObject() {
	if (selectedObject == NULL) return;
	setSelectedObject(getDisplacedObject(selectedObject->object->container));
}

int main(int argc, char **argv) {
	MathHelper::init();

	/* Audio and video init */
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_init() error: %s\n", SDL_GetError());
		exit (-1);
	}
	
	/* At program exit SDL_Quit will make a clean up */
    atexit(SDL_Quit);

	SDL_WM_SetCaption("SDL_test", NULL);
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

	initializeScene();
	
	bool alive = true;
	while (alive) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				alive = 0;
				break;
			case SDL_VIDEORESIZE:
				renderer->resize(event.resize.w, event.resize.h);
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
					alive = 0; break;
				case SDLK_UP:
					rotateSelectedObject(up, true); break;
				case SDLK_DOWN:
					rotateSelectedObject(up, false); break;
				case SDLK_RIGHT:
					rotateSelectedObject(right, true); break;
				case SDLK_LEFT:
					rotateSelectedObject(right, false); break;
				case SDLK_SPACE:
					autoRotate = !autoRotate;
					break;
				case SDLK_PERIOD:
					animator->tick(true); break;
				case SDLK_COMMA:
					animator->tick(false); break;
				case SDLK_s:
					setSelectedObject(objectSelection.next()); break;
				case SDLK_a:
					extendSelectedObject(); break;
				case SDLK_w:
					setWireframeMode(DSurface::RenderMode::WIREFRAME); break;
				case SDLK_e:
					setWireframeMode(DSurface::RenderMode::SURFACE); break;
				case SDLK_p:
					scene->print(std::cout); break;
				case SDLK_n:
					renderer->drawNormals = !renderer->drawNormals; std::cout << "renderer->drawNormals=" << renderer->drawNormals << std::endl; break;
				case SDLK_l:
					setNextLightingModel(); break;
				case SDLK_d:
					DrawingArea::displayDebugOutput = !DrawingArea::displayDebugOutput; break;
				default:
					break;
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				mousedrag_begin(event); break;
			case SDL_MOUSEBUTTONUP:
				mousedrag_end(event); break;
			case SDL_MOUSEMOTION:
				mousedrag_tick(event); break;
			}
		}
	
		if (autoRotate) animator->tick(true);
		renderer->render(*scene);
		
		if (DrawingArea::displayDebugOutput) alive = false;
		
		Uint32 delay = 1;
		if (renderer->lastRenderingTicks < ticksPerFrame) {
			delay = ticksPerFrame - renderer->lastRenderingTicks;
		}
		SDL_Delay(delay);
	}

	SDL_Quit();
	
	deleteScene();

	return 0;	
}

void addSelectableObject(DObjectCollection &scene, const std::string &name) {
	DObjectCollection::DisplacedObject *o = scene.findObject(name);
	if (o == NULL) return;
	objectSelection.push_back(o);
}





/*************************************************************************************************************
**************************************************************************************************************
*************************************************************************************************************/

void createCrossScene(DObjectCollection &scene);
void createTriangleScene(DObjectCollection &scene, bool addNormalVectors);
void createDoubleTriangleScene(DObjectCollection &scene);
void createSimpleSquareScene(DObjectCollection &scene);
void createSimpleCubeScene(DObjectCollection &scene);
void createTexturedCubeScene(DObjectCollection &scene);
void createDoubleCubeScene(DObjectCollection &scene);
void createSimpleCylinderScene(DObjectCollection &scene);
void createSimpleSpiralScene(DObjectCollection &scene);
void createSimpleSphereScene(DObjectCollection &scene);
void createBouncingBallsScene(DObjectCollection &scene);
void createTeapotScene(DObjectCollection &scene);

static Texture *exampleTexture;
static Texture *exampleBumpmapTexture;

void initializeScene() {
	lightingModels.push_back(std::make_pair(new SolidColorLighting(), "SolidColorLighting"));
	lightingModels.push_back(std::make_pair(new SimpleZLighting(), "SimpleZLighting"));
	lightingModels.push_back(std::make_pair(new ViewpointAngleLighting(), "ViewpointAngleLighting"));
	lightingModels.push_back(std::make_pair(new PixelAngleLighting(), "PixelAngleLighting"));
	lightingModels.push_back(std::make_pair(new InterpolatedNormalLighting(), "InterpolatedNormalLighting"));
	lightingModels.push_back(std::make_pair(new PhongLighting(), "PhongLighting"));
	lightingModels.push_back(std::make_pair(new TextureLighting(), "TextureLighting"));
	
	renderer = new Renderer(SCREEN_WIDTH, SCREEN_HEIGHT);
	SDL_PixelFormat *format = renderer->getSurface()->format;
	black = SDL_MapRGB(format, 0x00, 0x00, 0x00);
	white = SDL_MapRGB(format, 0xff, 0xff, 0xff);
	selected = SDL_MapRGB(format, 0xff, 0xbb, 0xbb);

	setNextLightingModel();
	animator = new SceneAnimator();
	objectSelection.push_back(NULL);
	scene = DObjectCollection::create();
	
	exampleTexture = new Texture("textures/paper.jpg");
	//exampleTexture->processBumpmap();
	exampleBumpmapTexture = new Texture("textures/chess.jpg");
	exampleBumpmapTexture->empower();
	exampleBumpmapTexture->processBumpmap();
	
	//DEBUG:
	//createTriangleScene(*scene, true);
	//createDoubleTriangleScene(*scene);
	//createSimpleSquareScene(*scene);
	//createDoubleCubeScene(*scene);
	
	//createCrossScene(*scene);
	//createSimpleCubeScene(*scene);
	//createTexturedCubeScene(*scene);
	//createSimpleCylinderScene(*scene);
	//createSimpleSpiralScene(*scene);
	//createSimpleSphereScene(*scene);
	//createBouncingBallsScene(*scene);
	createTeapotScene(*scene);
}

void deleteScene() {
	delete exampleBumpmapTexture;
	delete exampleTexture;
	delete animator;
	delete renderer;
	delete scene;
	
	for(LightingModelList::iterator i = lightingModels.begin(); i != lightingModels.end(); ++i) {
		delete i->first;
	}
}

static const int scale = 200;
static const int hscale = scale / 3;

void createCrossScene(DObjectCollection &scene) {
	const int d = static_cast<int>(hscale * 1.5f);
	DObjectCollection *vertical = DObjectCollection::create();
	Uint16 dimensions[3] = { hscale, hscale, hscale };
	vertical->add(DCube::create(dimensions, white), DObject::Direction(0, -d, 0), "v1");
	vertical->add(DCube::create(dimensions, white), DObject::Direction(0, 0, 0), "v2");
	vertical->add(DSphere::create(hscale, 50, 50, white), DObject::Direction(0, d, 0), "v3");
	//vertical.add(DCube::create(dimensions, white), DObject::Direction(0, d, 0), "v3");
	DObjectCollection *horizontal = DObjectCollection::create();
	horizontal->add(DCube::create(dimensions, white), DObject::Direction(-d, 0, 0), "h1");
	horizontal->add(DCube::create(dimensions, white), DObject::Direction(d, 0, 0), "h1");
	horizontal->add(vertical, "v");
	scene.add(horizontal, DObject::Direction(0, 0, 0), "h");

	addSelectableObject(scene, "h/v/v3");
	addSelectableObject(scene, "h/v");
	addSelectableObject(scene, "h");
	
	static const DObject::Rotation rotationv(0, M_2PI / 50, 0);
	static const DObject::Rotation rotationh(M_2PI / 110, M_2PI, M_2PI / 120);

	animator->addConstantAnimation(scene, "h", rotationh);
	animator->addConstantAnimation(scene, "h/v", rotationv);
	animator->addConstantAnimation(scene, "h/v/v3", rotationh);
}

void createTriangleScene(DObjectCollection &scene, bool addNormalVectors) {
	DSurfaceObject *o = new DSurfaceObject();
	DObject::Vertex v0(0, 30, 0);
	DObject::Vertex v1(scale, -scale/2, 0);
	DObject::Vertex v2(-scale, -scale/2, 0);
	if (addNormalVectors) {
		DSurface::NormalVector n0(0, 0, 100);
		DSurface::NormalVector n1(50, -100, 100);
		DSurface::NormalVector n2(-50, -100, 100);
		o->addSurface(v0, v1, v2, Material(white), n0, n1, n2);
	} else {
		o->addSurface(v0, v1, v2, Material(white));
	}
	scene.add(o, "triangle");
	addSelectableObject(scene, "triangle");
}

void createDoubleTriangleScene(DObjectCollection &scene) {
	const DObject::Vertex v0(0, scale, 0);
	const DObject::Vertex v1(scale, -scale, 0);
	const DObject::Vertex v2(-scale, -scale, 0);
	
	DSurfaceObject *o = new DSurfaceObject();
	o->addSurface(v0, v1, v2, Material(white));
	scene.add(o, DObject::Direction(-scale/2, 0, 0), "triangle1");
	
	o = new DSurfaceObject();
	o->addSurface(v0, v1, v2, Material(white));	
	scene.add(o, DObject::Direction(scale/2, 0, 0), "triangle2");
}
	
void createSimpleSquareScene(DObjectCollection &scene) {
	DSurfaceObject *o = new DSurfaceObject();
	DObject::Vertex v0(-hscale, hscale, 0);
	DObject::Vertex v1(hscale, hscale, 0);
	DObject::Vertex v2(hscale, -hscale, 0);
	DObject::Vertex v3(-hscale, -hscale, 0);
	o->addSquareSurface(v0, v1, v2, v3, white);
	scene.add(o, "square");
	addSelectableObject(scene, "square");
}
	
void createSimpleCubeScene(DObjectCollection &scene) {
	Uint16 dimensions[3] = { scale, scale, scale };
	scene.add(DCube::create(dimensions, white), "cube");
	addSelectableObject(scene, "cube");
}

void createTexturedCubeScene(DObjectCollection &scene) {
	Uint16 dimensions[3] = { scale, scale, scale };
	scene.add(DCube::create(dimensions, white, exampleTexture, exampleBumpmapTexture), "cube");
	addSelectableObject(scene, "cube");
	setLightingModel("TextureLighting");
}

void createDoubleCubeScene(DObjectCollection &scene) {
	Uint16 dimensions[3] = { scale, scale, scale };
	DObjectCollection *collection = DObjectCollection::create();
	const int d = static_cast<int>(hscale * 1.5f);
	collection->add(DCube::create(dimensions, white), DObject::Direction(-d, 0, 0), "cube1");
	collection->add(DCube::create(dimensions, white), DObject::Direction( d, 0, 0), "cube2");
	scene.add(collection, "boxes");
}

void createSimpleCylinderScene(DObjectCollection &scene) {
	scene.add(DCylinder::create(100, 70, 50, 13, white), "cylinder");
	addSelectableObject(scene, "cylinder");
}
	
void createSimpleSpiralScene(DObjectCollection &scene) {
	scene.add(DSpiral::create(200, 50, 20, 0.5f, 5, white), "spiral");
	addSelectableObject(scene, "spiral");
}
	
void createSimpleSphereScene(DObjectCollection &scene) {
	//DSphere *sphere = DSphere::create(hscale, 4, 2, white);
	//DSphere *sphere = DSphere::create(hscale, 5, 3, white);
	//DSphere *sphere = DSphere::create(hscale, 20, 20, white);
	DObject *sphere = DSphere::create(hscale, 10, 10, white);
	//scene.add(sphere, DObject::Direction(10, 0, 0), "sphere");
	scene.add(sphere, "sphere");
	addSelectableObject(scene, "sphere");
}

void createBouncingBallsScene(DObjectCollection &scene) {
	Uint16 dimensions[3] = { scale*1.5f, scale, scale*1.5f };
	const int ballRadius = scale / 9;
	static const DObject::Rotation rotationh(M_2PI / 110, M_2PI, M_2PI / 120);

	DObjectCollection *box = DObjectCollection::create();
	
	/*DObject *cube = DCube::create(dimensions, white);
	cube->setRenderMode(DSurface::RenderMode::WIREFRAME);
	box->add(cube, "boundingbox");*/
	
	Uint16 groundDimensions[3] = { scale*1.5f, 5, scale*1.5f };
	box->add(
		DCube::create(groundDimensions, white)
		, Displacement(Displacement::Direction(0, -dimensions[1]/2,0), Displacement::Rotation::zero)
		, "ground"
	);
	
	for(int i = 0; i < 5; ++i) {
		DObjectCollection::DisplacedObject *ball = box->add(DSphere::create(ballRadius, 10, 10, white), "ball");
		animator->addAnimation(
			new BouncingAnimation(
				ball
				, DObject::Direction(dimensions[0]/2 - ballRadius, dimensions[1]/2 - ballRadius, dimensions[2]/2 - ballRadius)
				, DObject::Direction(i, 10 + i, i * 2)
			)
		);
		animator->addAnimation(
			new ConstantAnimation(
				ball
				, Displacement(Displacement::Direction::zero, rotationh)
			)
		);
		objectSelection.push_back(ball);
	}
	scene.add(box, "h");
	addSelectableObject(scene, "h");
	animator->addConstantAnimation(scene, "h", DObject::Rotation(0, M_2PI / 300, 0));
}

void createTeapotScene(DObjectCollection &scene) {
	Displacement d(Displacement::Direction::zero, Displacement::Rotation(M_2PI / 10, 0, 0));
	scene.add(createTeapot(white, exampleTexture, exampleBumpmapTexture), d, "teapot");
	addSelectableObject(scene, "teapot");
	static const DObject::Rotation rotationv(0, M_2PI / 250, 0);
	animator->addConstantAnimation(scene, "teapot", rotationv);
	
	setSelectedObject(scene.findObject("teapot"));
	
	setLightingModel("InterpolatedNormalLighting");
	//setLightingModel("PhongLighting");
}
