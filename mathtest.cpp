#include <iostream>

#include "util.h"
#include <cmath>

void verify(float f) {
	std::cout << f << "\t: " << MathHelper::acos(f) << " \t" << ::acos(f) << std::endl;
}

int main() {
	float step = 0.001;
	MathHelper::init();
	
	
	for(float f = -1; f <= 1; f += step) {
		verify(f);
	}
	
	verify(-1);
	verify(0);
	verify(1);
}
