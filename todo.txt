todo:

DSurfaceObject
	//TODO: the object should contain a set of all vertexes, so that rotations can be done on the 
	//       set instead of on multiple copies of each vertex.

main
	//TODO: The rotations/displacements should be entered into a sceneanimator
	//       so that the main process won't have to keep track of each objects rotation.

MDTriangle
	// TODO: The template argument should be the vertex type, so that the triangle
	//        won't neccessarily be bound to using a MDVector

Renderer
	//TODO: Surface normal calculation should be cached somehow
	//TODO: Vertexes that have a normal with z <= 0 should be removed from the list
	//       prior to sorting.

