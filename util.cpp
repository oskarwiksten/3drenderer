#include "util.h"

#include <SDL/SDL.h>
#include <cmath>
#include <cassert>

#define T Uint16
#define M ((1 << (sizeof(T) * 8)) - 1)
#define halfM ((M) / 2)

MathHelper::Precision sinTable[M];
MathHelper::Precision cosTable[M];
MathHelper::Precision acosTable[M];

void MathHelper::init() {
	sinTable[0] = static_cast<Precision>(std::sin(0));
	cosTable[0] = static_cast<Precision>(std::cos(0));
	acosTable[0] = static_cast<Precision>(std::acos(-1));
	for(T i = 1; i != 0; ++i) {
		const float f = static_cast<float>(i) * M_2PI / M;
		sinTable[i] = static_cast<Precision>(std::sin(f));
		cosTable[i] = static_cast<Precision>(std::cos(f));
		const float v = static_cast<float>(i - halfM) / halfM;
		acosTable[i] = static_cast<Precision>(std::acos(v));
	}
}

MathHelper::Precision MathHelper::sin(float a) {
	T i = static_cast<T>(a * static_cast<Precision>(M) / M_2PI);
	return sinTable[i];
}

MathHelper::Precision MathHelper::cos(float a) {
	T i = static_cast<T>(a * static_cast<Precision>(M) / M_2PI);
	return cosTable[i];
}

MathHelper::Precision MathHelper::acos(float a) {
	//return ::acos(a);
	
	T i = static_cast<T>(halfM + a * halfM);
	return acosTable[i];
}

void ind(std::ostream &os, unsigned int indent) {
	for(unsigned int i = 0; i < indent; ++i) os << " ";
}

const unsigned int factorial(const unsigned int &i) {
	if (i <= 1) return 1;
	else return i * factorial(i - 1);
}

unsigned int choose(const unsigned int a, const unsigned int b) {
	assert(a >= b);
	return factorial(a) / (factorial(b) * factorial(a - b));
}
