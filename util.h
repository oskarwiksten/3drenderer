#ifndef __util_h__
#define __util_h__

#include <cmath>
#define M_2PI (2 * (M_PI))

#define MIN(a,b) ((a) < (b) ? (a) : (b))
//#define MIN(a,b,c) (MIN(MIN(a,b),c))
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define DIFF(a,b) (static_cast<PixelCoordinateDistance>(a) - static_cast<PixelCoordinateDistance>(b))

class MathHelper {
public:
	typedef float Precision;
	static void init();
	static Precision sin(float a);
	static Precision cos(float a);
	static Precision acos(float a);
};

#include <iostream>

void ind(std::ostream &os, unsigned int indent);

#include <vector>

template<class T>
class RotateSelection : public std::vector<T> {
private:
	size_t selectedIndex;
public:
	RotateSelection() : selectedIndex(0) {}
	const T& next() { 
		++selectedIndex;
		if (selectedIndex >= std::vector<T>::size()) selectedIndex = 0;
		return current();
	}
	const T& current() const { 
		return (*this)[selectedIndex];
	}
};



const unsigned int factorial(const unsigned int &i);
unsigned int choose(const unsigned int a, const unsigned int b);

template<class T>
T pow(const T &u, int i) {
	if (i == 0) return 1;
	if (u == 0) return 0;
	if (i > 0) {
		return u * pow(u, i-1);
	} else {
		return static_cast<T>(1) / pow(u, -i);
	}
}

template<class T>
T bernstein(const int i, const T &u) {
	return static_cast<T>(choose(static_cast<unsigned int>(3), i)) * pow(u, i) * pow(1-u, 3-i);
}

template<class T>
T bernstein_d(const int i, const T &u) {
	const T nu = static_cast<T>(1) - u;
	// b' = f_i * ( i(1-u)^(3-i) * x^(i-1) - (3-i)(1-x)^(2-i) * x^i)
	
	return static_cast<T>(choose(static_cast<unsigned int>(3), static_cast<unsigned int>(i))) 
			* (
				static_cast<T>(i)*pow(nu, 3-i) * pow(u, i-1) - static_cast<T>(3-i)*pow(nu, 2-i) * pow(u,i)
			);
	
	/*
	T a = static_cast<T>(choose(static_cast<unsigned int>(3), static_cast<unsigned int>(i)));
	T b = static_cast<T>(i)*pow(nu, 3-i) * pow(u, i-1);
	T c = static_cast<T>(3-i)*pow(nu, 2-i) * pow(u,i);
	T c1 = static_cast<T>(3-i);
	T c2 = pow(nu, 2-i);
	T c3 = pow(u,i);
	c = c1 * c2 * c3;
	
	if (std::isnan(a) || std::isinf(a)) std::cout << "(1) i=" << i << " resulted in nan." << std::endl;
	if (std::isnan(b) || std::isinf(b)) std::cout << "(2) i=" << i << " u=" << u << " resulted in nan." << std::endl;
	if (std::isnan(c) || std::isinf(c)) {
		std::cout << "(3) i=" << i << " u=" << u << " resulted in nan." << std::endl;
		std::cout << "c1=" << c1 << " c2=" << c2 << " c3=" << c3 << std::endl;
	}
	if (std::isnan(c1)) std::cout << "(c1)" << std::endl;
	if (std::isnan(c2)) std::cout << "(c2)" << std::endl;
	if (std::isnan(c3)) std::cout << "(c3)" << std::endl;
	
	return a * (b - c);
	*/
}

#endif
